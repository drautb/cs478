<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

CS 478 - Machine Learning
=========================

# Monday 9/23/13

One of the huge assumptions that Naive Bayes makes is that all of the attributes are independent of one another. This prevents us having to calculate a joint probability distribution.

$argmax P(v_j)\prod_{i=1}^{n} P(a_i|v_j)$


# Friday 9/20/13

## Naive Bayes

_Naive Bayes_ is a model that is kind of in between ID3 and KNN. It does generate a model, but it doesn't need to be completely rebuilt if you wish to add new data.

> Bayesian Reasoning provides a probabalistic approach to reasoning.

**Bayes Theorem:**

$P(h|D) = \frac{P(D|h)P(h)}{P(D)}$

# Wednesday 9/18/13

## Instance-Based Learning 

* AKA K-Nearest Neighbors
* Instance-based learning doesn't generate a generalized model, it just uses the training data _as_ the model. It classifies new data based on whichever traning instance it matches most closely.
    * Easy to add new data for classifying, but execution can be expensive. (Determing closest neighbor in the Voronoi space can be costly)
* In reality, you normally see how 3 or 5 of its closest neighbors are classified.
* Remarks
    * Fairly noise tolerant
    * Lots of irrelevant attributes hurt performance, add computation.
    * For real problems, relies on efficient indexing.

# Monday 9/16/13

## Project Ideas
* Autism Data
* Traffic Analysis
* Something with Twitter...mood by region? Time of day? 
    * Retweets of Mormon.org tweets by region? Age?


# Friday 9/13/13

* WEKA Stuff

# Wednesday, Sep. 11, 2013

### Opening Quiz
* 1) 30 - 60
* 2) 900 - 10,000
* 3) 10 - 30
* 4) 10 - 30
* 5) 1,000 - 100,000
* 6) 10,000 - 1,000,000
* 7) 1700 - 1750 
* 8) 300 - 800 Days
* 9) 20,000 - 30,000 Miles
* 10) 33,000 - 39,000 Feet

Why didn't we all just give range $-\infty to \infty$?

## Entropy

Let S be a set examples from c classes

$Entropy(s) = \sum_{i = 1}^{c} -p_ilog_2p_i$

Where $p_i$ is the proportion of examples of $S$ belonging to class $i$. (We define $0 log0 = 0$)

We use entropy as a faster surrogate than accuracy when trying to determine if our mechanism is improving. If we use accuracy, it grows exponentially.


# Monday, Sep. 9, 2013

## Decision Trees

Typically used for deductive logic, but a basic machine learning algorithm will try to induce the tree based on examples given.
When trying to induce the tree, the algorithm will reach either a point where it has classified everything, or it doesn't know what else to ask.

We can measure _entropy_ in a learning algorithm to give us confidence in what we know.

### Top Down Inductive Decision Tree

Greedy, Divide and Conquer approach.

_Induce-Tree_
* Pick a property _P_
* Remove it from _Properties_
* Make _P_ the root of the current tree
* For each value _V_ of _P_
 * Create a branch of the current tree labeled by _V_
 * Create _Partition-V_, elements of example set with value _V_ for _P_
 * _Induce-Tree(Partition-V, Properties)_
 * Attach the result to branch _V_

**Potential Problems:**
* Choosing the wrong _P_ early on could make it difficult to find anything later on.
* No way of absorbing noise.
* Some attributes may not matter at all, but this will try to infer meaning from them anyways.
* As the tree deepens, your sample size shrinks.
* Overfitting.

**Mechanisms for Improvement:**
* Stop going deeper when sample size gets too small.
* Prune branches out afterwards if they hurt performance. (Post-pruning)

> **Post-Pruning**

> Let the algorithm run completely, don't stop it at all. Then, using another set of data that you held aside, use the tree to try to classify it.
> The tree may predict with some degree of accuracy, say 85%. Now, try removing some of the smaller branches in the tree, and re-run the exercise.
> Typically, this will improve the accuracy of it's predictions.
> * Tends to be more common than pre-pruning.
> * Is used frequently, even with other techniques, to improve accuracy. Hindsight is 20/20.


# Friday, Sep. 6, 2013

## Bias
**Two big points:**
* Patterns observed in the past may not always predict the future.
* A bias is required to learn

**Without bias, the _best_ we can do is _memorize_, no generalizations.**

>**Conceptual Proof**

>We need a language for represent individual observations, as well as a separate language for representing sets in our universe.
>(Observe one French person, form generalization about the set of all French)

>English isn't rich enough to characterize the set of all possible subsets. Imagine we had one. (Unbiased Generalization Language) 
>Such a language has no bias, every possible subset has a name.

>So, if we have this language, we can exactly classify everything, we don't generalize at all. If this is the case,
>we only classify what we have seen. We have no power to generalize. We can only make 'predictions' about what we 
>have already seen, which is just memorization. No learning takes place.

>**Formally:** (Check slides for proofs)

>*Lemma 1:* Any new instance, NI, is classified as positive if and only if NI is identical to some previously observed positive instance.

>*Lemma 2:* Any new instance, NI, is classified as negative if and only if NI is identical to some previously observed negative instance.

>*Lemma 3:* If NI is any instance which was not observed, then NI matches exactly one half of VS, and so cannot be classified.
>(If I haven't seen it before, I can't classify it.)

>*Theorem:* An unbiased generalization procedure can never make the inductive leap neccessary to classify instances beyond those it has observed.

## Sources of Bias in Learning
Either the representation language, or the generalization procedure is usually biased, or both.

Generalization Procedure Examples:
* Domain Knowledge (e.g. double bonds don't break easily)
* Intended Uses
* Shared Assumptions (Talking about "crowns" in a dentistry class vs. a history class)
* Common sense







