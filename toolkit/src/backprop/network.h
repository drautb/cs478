#ifndef BACKPROPAGATION_NEURAL_NETWORK_H
#define BACKPROPAGATION_NEURAL_NETWORK_H

#include <iostream>
#include <time.h>
#include <sys/time.h>

#include "../matrix.h"
#include "../rand.h"
#include "../learner.h"

#include "layer.h"
#include "node.h"

using namespace std;

double getTimeInSeconds()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double)tp.tv_sec + (double)tp.tv_usec * 1e-6);
}

class BackpropagationNeuralNetwork
{
private:

    /**
     * Then number of input nodes, output nodes, and layers.
     * The vector stores the number of nodes per hidden layer.
     */
    int nInput, nOutput, nLayers;
    vector<int> nPerLayer;

    Layer* inputLayer;
    Layer* outputLayer;

    vector<Layer*> hiddenLayers;

    Rand* r;

    /**
     * Calculation Values
     */
    double epochError, lastEpochError, deltaErrorThreshold;
    double startTime, endTime;
    int currentEpoch;

    double learningRate, momentum;

    const static int TRAINING_TIME_LIMIT = 60; // seconds
    const static int TRAINING_EPOCH_LIMIT = 10000; // Number of epochs allowed to train

public:

    BackpropagationNeuralNetwork(int nInput, int nOutput, int nLayers, 
                                 const vector<int>& nPerLayer,
                                 double learningRate, double momentum,
                                 Rand& r)
    {
        this->learningRate = learningRate;
        this->momentum = momentum;
        this->r = &r;

        cout << endl << "\tCreating Neural Network with the following Layers:" << endl;
        cout << "\tInputs\t"<< nInput << endl;
        cout << "\t-----------------" << endl;
        for (size_t l=0; l<nPerLayer.size(); l++)
            cout << "\t\t" << nPerLayer[l] << endl;
        cout << "\t-----------------" << endl;
        cout << "\tOutputs\t" << nOutput << endl;

        // Create the network structure
        inputLayer = new Layer(nInput);
        outputLayer = new Layer(nOutput);

        hiddenLayers.clear();
        for (int l=0; l<nLayers; l++)
            hiddenLayers.push_back(new Layer(nPerLayer[l]));

        // Connect the network structure
        double MIN_WEIGHT = -0.5;
        double MAX_WEIGHT = 0.5;

        if (hiddenLayers.size() == 0) 
        {
            inputLayer->ConnectForward(outputLayer);
            inputLayer->RandomizeWeights(r, MIN_WEIGHT, MAX_WEIGHT);
        }
        else
        {
            inputLayer->ConnectForward(hiddenLayers[0]);
            inputLayer->RandomizeWeights(r, MIN_WEIGHT, MAX_WEIGHT);
            for (size_t l=0; l<hiddenLayers.size(); l++)
            {
                if (l == hiddenLayers.size() - 1) 
                {
                    hiddenLayers[l]->ConnectForward(outputLayer);
                    hiddenLayers[l]->RandomizeWeights(r, MIN_WEIGHT, MAX_WEIGHT);
                }
                else 
                {
                    hiddenLayers[l]->ConnectForward(hiddenLayers[l+1]);
                    hiddenLayers[l]->RandomizeWeights(r, MIN_WEIGHT, MAX_WEIGHT);
                }
            }
        }
    }

    ~BackpropagationNeuralNetwork()
    {
        delete inputLayer;
        delete outputLayer;

        for (size_t l=0; l<hiddenLayers.size(); l++)
            delete hiddenLayers[l];
        
        hiddenLayers.clear();
    }

    void Train(Matrix& features, Matrix& labels, Matrix& testFeatures, Matrix& testLabels)
    {
        deltaErrorThreshold = 0.001;
        lastEpochError = 2.0;
        epochError = 1.0;
        currentEpoch = 1;
        startTime = getTimeInSeconds();

        cout << endl;
        // cout << "JSON STRING:" << endl;
        // cout << "{\"learningrate\": " << learningRate << ", \"momentum\": " << momentum << ", ";
        // cout << "\"layers\": [ ";
        // for (size_t l=0; l<hiddenLayers.size(); l++) 
        // {
        //     if (l==0)
        //         cout << hiddenLayers[l]->nodes.size();
        //     else
        //         cout << ", " << hiddenLayers[l]->nodes.size();
        // }
        // cout << " ], \"epochdata\": [";

        // Loop until the epochError is < some value || time limit is exceeded || some number of epochs has passed
        while (abs(lastEpochError - epochError) > deltaErrorThreshold)
        {
            // cout << "\t**** NEW EPOCH: " << currentEpoch << " ****" << endl;
            // cout << "\tEPOCH: " << currentEpoch << endl;
            lastEpochError = epochError;
            epochError = 0.0;

            features.shuffleRows(*r, &labels);

            // For each example in the training set
            for (size_t r=0; r<features.rows(); r++)
            {
                // cout << "New Example. Current Epoch Error: " << epochError << endl;
                // Set up the network
                for (size_t i=0; i<inputLayer->nodes.size(); i++)
                    inputLayer->nodes[i]->output = features.row(r)[i];

                for (size_t o=0; o<outputLayer->nodes.size(); o++)
                {
                    if (labels.row(r)[0] == o)
                        outputLayer->nodes[o]->target = 1.0;
                    else
                        outputLayer->nodes[o]->target = 0.0;
                }

                // Inputs and targets are set, so run the prop and backprop stuff
                PropagateAndBackprop();

                // exit(2);
            }

            epochError *= 0.5;
            // cout << "\tEpoch " << currentEpoch << " complete, error: " << epochError << endl;

            // if (currentEpoch == 1)
            //     cout << "{ \"n\": " << currentEpoch << ", ";
            // else
            //     cout << ",{ \"n\": " << currentEpoch << ", ";


            int right = 0;
            for (size_t e=0; e<features.rows(); e++)
            {
                if (this->PredictClass(features.row(e)) == labels.row(e)[0])
                    right++;
            }
            double accuracy = (double)right / (double)features.rows();
            // cout << "\t\tTrain: " << accuracy << endl;
            // cout << "\"training\": " << accuracy << ", ";

            right = 0;
            for (size_t e=0; e<testFeatures.rows(); e++)
            {
                if (this->PredictClass(testFeatures.row(e)) == testLabels.row(e)[0])
                    right++;
            }
            accuracy = (double)right / (double)testFeatures.rows();
            // cout << "\t\tTest: " << accuracy << endl;
            // cout << "\"test\": " << accuracy << " } ";

            //Check for Epoch limit
            if (++currentEpoch > TRAINING_EPOCH_LIMIT)
            {
                // cout << "] }" << endl << endl;;
                cout << "\t\tTraining Epoch Limit of " << TRAINING_EPOCH_LIMIT << " exceeded, stopping." << endl;
                break;
            }

            // Check for time
            if (getTimeInSeconds() - startTime > TRAINING_TIME_LIMIT)
            {
                // cout << "] }" << endl << endl;
                cout << "\tTraining Time of " << TRAINING_TIME_LIMIT << " seconds exceeded, stopping." << endl;
                break;
            }
        }

        if (abs(lastEpochError - epochError) < deltaErrorThreshold)
        {
            // cout << "] }" << endl << endl;
            cout << "\n\tDelta in Epoch Error less than " << deltaErrorThreshold << " after " << currentEpoch << " epochs, stopping." << endl;
        }

        cout << endl << "STATS:" << endl;
        cout << "\tError in Model: " << epochError << endl;
        cout << "\tNumber of Epochs: " << currentEpoch << endl;
        cout << "\tLast Delta Error: " << (lastEpochError - epochError) << endl;
        cout << "\tTime to Train: " << (getTimeInSeconds() - startTime) << endl;

        // cout << "Weights:" << endl;
        // inputLayer->PrintWeights();
        // for (size_t l=0; l<hiddenLayers.size(); l++)
        // {
        //     hiddenLayers[l]->PrintWeights();
        // }
    }

    size_t PredictClass(const std::vector<double>& features)
    {
        for (size_t i=0; i<inputLayer->nodes.size(); i++)
            inputLayer->nodes[i]->output = features[i];

        for (size_t l=0; l<hiddenLayers.size(); l++)
            hiddenLayers[l]->CalculateOutputs();

        outputLayer->CalculateOutputs();            

        return outputLayer->HighestOutputNode();
    }

    void PropagateAndBackprop()
    {
        // We assume that each input node has it's value put in right now.
        // We also assume that output nodes also know their target value.
        // So we propgate forward
        
        // cout << "Starting PropagateAndBackprop..." << endl;

        for (size_t l=0; l<hiddenLayers.size(); l++)
        {
            // cout << "\tHidden Layer: " << l << endl;
            hiddenLayers[l]->CalculateOutputs();
        }

        // cout << "\tOutput Layer: " << endl;
        outputLayer->CalculateOutputs();

        // Forward movement is complete. So we'll calculate the errors 
        // in each of the outputs, and the hidden nodes.

        outputLayer->CalculateErrors();

        // Update the epoch error value
        epochError += outputLayer->SumErrorsSquared();

        for (int l=hiddenLayers.size() - 1; l>=0; l--)
            hiddenLayers[l]->CalculateErrors();

        // Update the weights in the net
        inputLayer->UpdateWeights(learningRate, momentum);
        for (size_t l=0; l<hiddenLayers.size(); l++)
            hiddenLayers[l]->UpdateWeights(learningRate, momentum);
    }

};

#endif
