#include "node.h"
#include "layer.h"

Node::Node()
{
    parents.clear();
    children.clear();
    lastDeltaWeights.clear();
    Reset();
}

Node::~Node()
{

}

void Node::ConnectForward(Layer* fl)
{
    for (unsigned int n=0; n<fl->nodes.size(); n++)
    {
        children.push_back(pair<double, Node*>(0.0, fl->nodes[n]));
        lastDeltaWeights.push_back(0.0);
        fl->nodes[n]->parents.push_back(this);
    }
}

void Node::Reset()
{
    output = 0.0;
    target = 0.0;
    error = 0.0;
    deltaWeight = 0.0;
}

void Node::RandomizeWeights(Rand& r, double min, double max)
{
    for (unsigned int n=0; n<children.size(); n++)
    {
        // Calculate random weight
        children[n].first = min + r.uniform() * (max - min);
        // children[n].first = 0.5;
    }
}

void Node::CalculateOutput(int nodeIdx)
{
    // Calculate weighted sum
    output = 0.0;
    for (unsigned int p=0; p<parents.size(); p++)
    {
        // Parent Node's Output * The weight of the edge to this node
        output += parents[p]->output * parents[p]->children[nodeIdx].first;
    }

    // Apply sigmoid function
    output = 1 / (1 + exp(-output));
    // cout << "\t\tOutput for node " << nodeIdx << ": " << output << endl;
}

void Node::CalculateError()
{
    // Output Node Error
    if (children.empty())
    {
        error = output * (1 - output) * (target - output);
        return;
    }

    // Hidden Node Error
    double errSum = 0.0;
    for (size_t c=0; c<children.size(); c++)
        errSum += children[c].first * children[c].second->error;

    error = output * (1 - output) * errSum;
}

void Node::UpdateWeights(double learningRate, double momentum)
{
    for (size_t c=0; c<children.size(); c++)
    {
        deltaWeight = learningRate * children[c].second->error * this->output;   
        children[c].first += deltaWeight + momentum * lastDeltaWeights[c];
        lastDeltaWeights[c] = deltaWeight;   
    }
}

void Node::PrintWeights()
{
    cout << "\tNode Weights:" << endl;
    cout << "\t["<< endl;

    for (size_t c=0; c<children.size(); c++)
        cout << "\t\t" << children[c].first << endl;

    cout << "\t]" << endl;
}
