/**
 * BackpropagationLearner
 * @author Ben Draut
 * @date 20 Oct 2013
 */

#ifndef BACKPROPAGATION_LEARNER_H
#define BACKPROPAGATION_LEARNER_H

#include <cmath>
#include <iostream>
#include <sstream>
#include <memory>
#include <algorithm>
#include <vector>

#include "../learner.h"
#include "../rand.h"
#include "../error.h"

#include "network.h"

using namespace std;

class BackpropagationLearner : public SupervisedLearner
{
private:

    Rand& m_rand;

    const vector<int>* nPerLayer;

    double learningRate, momentum;

    BackpropagationNeuralNetwork* network;

public:
    BackpropagationLearner(Rand& r, vector<int>* nPerLayer, double lr, double m)
    : SupervisedLearner(), m_rand(r), nPerLayer(nPerLayer), learningRate(lr), momentum(m)
    {
        network = NULL;   
    }

    virtual ~BackpropagationLearner()
    {
    }

    /**
     * Train. This generates our predictive model, a backpropagation neural netowrk.
     * @param features A Matrix of samples. Each row is a sample, and each column in that
     *                 row represents an attribute.
     * @param labels   A Matrix of classes or lables. This is the "correct" classification for 
     *                 the corresponding sample row in features.
     */
    virtual void train(Matrix& features, Matrix& labels, Matrix& testFeatures, Matrix& testLabels)
    {
        // cout << "FEATURES:\n" << features.toString(true) << endl;

        // Make sure that there is a label for each sample
        if(features.rows() != labels.rows())
            ThrowError("Expected the features and labels to have the same number of rows");

        features.shuffleRows(m_rand, &labels);

        // Throw away any previous training
        if (network != NULL) 
        {
            delete network;
            network = NULL;
        }

        network = trainNetwork(features, labels, testFeatures, testLabels);
    }

    // Evaluate the features and predict the labels
    virtual void predict(const std::vector<double>& features, std::vector<double>& labels)
    {
        labels[0] = network->PredictClass(features);
    }

private:

    BackpropagationNeuralNetwork* trainNetwork(Matrix& features, Matrix& labels, Matrix& testFeatures, Matrix& testLabels) 
    {
        // Figure out the parameters
        int nInputs = features.cols();
        int nOutpus = labels.valueCount(0);

        int layers = nPerLayer->size();

        // Create a NN
        BackpropagationNeuralNetwork* nn = new BackpropagationNeuralNetwork(nInputs, 
                                                                            nOutpus, 
                                                                            layers, 
                                                                            *nPerLayer,
                                                                            learningRate,
                                                                            momentum,
                                                                            m_rand);

        nn->Train(features, labels, testFeatures, testLabels);                

        return nn;
    }
};


#endif // BACKPROPAGATION_LEARNER_H
