#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <vector>
#include <cmath>

#include "../rand.h"
#include "layer.h"

using namespace std;

class Layer;

class Node
{
public:

    /**
     * Vector of pointers to all parent nodes
     */
    vector<Node*> parents;

    /**
     * Vector of pointers to all child nodes,
     * as well as the edge weight to each one.
     */
    vector< pair<double, Node*> > children;

    /**
     * Another vector of the last delta weights for each edge.
     * This is for momentum.
     */
    vector<double> lastDeltaWeights;

    /**
     * Store the 'output' of this node
     */
    double output;

    double target; // Just used for output nodes

    double error;

    double deltaWeight;

    const static int WEIGHT = 0;
    const static int NODE = 1;

public:

    Node();

    ~Node();

    void ConnectForward(Layer* fl);

    void Reset();

    void RandomizeWeights(Rand& r, double min, double max);

    void CalculateOutput(int nodeIdx);

    void CalculateError();

    void UpdateWeights(double lr, double m);

    void PrintWeights();

};

#endif

