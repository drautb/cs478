#include <cmath>

#include "layer.h"
#include "node.h"

Layer::Layer(int nNodes)
{
    for (int n=0; n<nNodes; n++)
        nodes.push_back(new Node());
}

Layer::~Layer()
{
    for (unsigned int n=0; n<nodes.size(); n++) 
    {
        if (nodes[n] != NULL)
        {
            delete nodes[n];
            nodes[n] = NULL;
        }

        nodes.clear();
    }
}

/**
 * This method sets up all the connections to connect this layer 
 * forward to another layer.
 * @param fl An existing layer to connect to.
 */
void Layer::ConnectForward(Layer* fl)
{
    for (unsigned int n=0; n<nodes.size(); n++) 
    {
        nodes[n]->ConnectForward(fl);
    }
}

void Layer::RandomizeWeights(Rand& r, double min, double max)
{
    for (unsigned int n=0; n<nodes.size(); n++)
    {
        nodes[n]->RandomizeWeights(r, min, max);
    }
}

void Layer::CalculateOutputs()
{
    for (unsigned int n=0; n<nodes.size(); n++)
    {
        nodes[n]->CalculateOutput(n);
    }
}

void Layer::CalculateErrors()
{
    for (size_t n=0; n<nodes.size(); n++)
        nodes[n]->CalculateError();
}

double Layer::SumErrorsSquared()
{
    double sum = 0.0;
    for (size_t n=0; n<nodes.size(); n++)
        sum += pow((nodes[n]->target - nodes[n]->output), 2);

    return sum;
}

void Layer::UpdateWeights(double lr, double m)
{
    for (size_t n=0; n<nodes.size(); n++)
        nodes[n]->UpdateWeights(lr, m);
}

size_t Layer::HighestOutputNode()
{
    double greatestOutputSoFar = 0.0;
    size_t greatestIdx = 0;
    // cout << endl;
    for (size_t n=0; n<nodes.size(); n++)
    {
        // cout << "Output[" << n << "]: " << nodes[n]->output << endl;
        if (nodes[n]->output > greatestOutputSoFar)
        {
            greatestOutputSoFar = nodes[n]->output;
            greatestIdx = n;
        }
    }

    return greatestIdx;
}

void Layer::PrintWeights()
{
    cout << "LAYER WEIGHTS:" << endl;
    for (size_t n=0; n<nodes.size(); n++)
        nodes[n]->PrintWeights();
}
