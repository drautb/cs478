#ifndef LAYER_H
#define LAYER_H

#include <iostream>
#include <vector>

#include "node.h"
#include "../rand.h"

using namespace std;

class Node;

class Layer 
{
public:

    vector<Node*> nodes;

public:

    Layer(int nNodes);

    ~Layer();

    /**
     * This method sets up all the connections to connect this layer 
     * forward to another layer.
     * @param fl An existing layer to connect to.
     */
    void ConnectForward(Layer* fl);

    void RandomizeWeights(Rand& r, double min=-1.0, double max=1.0);

    void CalculateOutputs();

    void CalculateErrors(); 

    double SumErrorsSquared();

    void UpdateWeights(double lr, double m);

    size_t HighestOutputNode();

    void PrintWeights();

};

#endif

