/**
 * DecisionTreeLearner
 * @author Ben Draut
 * @date 20 Sep 2013
 */

#ifndef DECISION_TREE_H
#define DECISION_TREE_H

#include <cmath>
#include <iostream>
#include <sstream>
#include <memory>
#include <algorithm>

#include "../learner.h"
#include "../rand.h"
#include "../error.h"
#include "decisionnode.h"

using namespace std;

/**
 * This is a decision tree learner that implements the ID3 
 * decision tree algorithm
 */
class DecisionTreeLearner : public SupervisedLearner
{
private:

    Rand& m_rand;

    /**
     * Possible Values:
     * "information" => Using entropy
     * "accuracy" => Using accuracy
     * "extended"
     */
    string splittingMethod;

    /**
     * Data structure for the prediction tree
     */
    DecisionNode* ID3DecisionTree;

public:
    DecisionTreeLearner(Rand& r)
    : SupervisedLearner(), m_rand(r)
    {
        ID3DecisionTree = NULL;
        splittingMethod = "information"; // Default is information gain
    }

    virtual ~DecisionTreeLearner()
    {
    }

    /**
     * Train. This generates our predictive model, which is
     * a decision tree in this case.
     * @param features A Matrix of samples. Each row is a sample, and each column in that
     *                 row represents an attribute.
     * @param labels   A Matrix of classes or lables. This is the "correct" classification for 
     *                 the corresponding sample row in features.
     */
    virtual void train(Matrix& features, Matrix& labels, Matrix& testFeatures, Matrix& testLabels)
    {
        // cout << "FEATURES:\n" << features.toString(true) << endl;

        // Make sure that there is a label for each sample
        if(features.rows() != labels.rows())
            ThrowError("Expected the features and labels to have the same number of rows");

        features.shuffleRows(m_rand, &labels);

        // Throw away any previous training
        if (ID3DecisionTree != NULL) 
        {
            delete ID3DecisionTree;
            ID3DecisionTree = NULL;
        }

        //Time to build the decision tree.
        vector<size_t> properties;
        properties.clear();

        // populate properties. Each property is just a column index.
        for (size_t p=0; p<features.cols(); p++)
            properties.push_back(p);

        ID3DecisionTree = induceTree(features, labels, properties);
    }

    // Evaluate the features and predict the labels
    virtual void predict(const std::vector<double>& features, std::vector<double>& labels)
    {
        // cout << "Predicting For Row: ";
        // for (size_t i=0; i<features.size(); i++)
        //     cout << " " << features[i] << ", ";
        // cout << endl;
        labels[0] = ID3DecisionTree->predictClass(features);
    }

    /**
     * Helper to set the splitting method
     */
    void setSplittingMethod(const string& splittingMethod){this->splittingMethod = splittingMethod;}

private:

    /**
     * This method is recursively generates an ID3 decision tree from the given data.
     * @param  features   The example set
     * @param  labels     The classifications of each instance in the example set
     * @param  properties The list of properties that we can split on
     * @return            A complete ID3 tree
     *
     * Function Induce-Tree(Example-set, Properties)
     *  If all elements in Example-set are in the same class, then return a leaf node labeled with that class
     *  Else if Properties is empty, then return a leaf node labeled with the majority class in Example-set
     *  Else
     *      Select P from Properties  (*)
     *      Remove P from Properties
     *      Make P the root of the current tree
     *      For each value V of P
     *          Create a branch of the current tree labeled by V
     *          Partition_V  Elements of Example-set with value V for P
     *          Induce-Tree(Partition_V, Properties)
     *          Attach result to branch V
     */
    DecisionNode* induceTree(Matrix& features, Matrix& labels, vector<size_t> properties)
    {
        // If all elements in Example-set are in the same class, then return a leaf node labeled with that class
        // Else if Properties is empty, then return a leaf node labeled with the majority class in Example-set
        if (labels.columnIsUniform(0))
        {
            // cout << "\tUNIFORM, New Leaf Node: " << labels.attrValue(0, labels[0][0]) << endl;
            return new DecisionNode(0, labels[0][0]);
        }
        else if (properties.empty())
        {
            // cout << "\tNO MORE PROPERTIES, New Leaf Node: " << labels.attrValue(0, labels.mostCommonValue(0)) << endl;
            return new DecisionNode(0, labels.mostCommonValue(0));
        }

        size_t propertyCol = chooseProperty(features, labels, properties);
        properties.erase(remove(properties.begin(), properties.end(), propertyCol), properties.end());

        // cout << "SPLITTING ON: " << features.attrName(propertyCol) << endl;

        DecisionNode* root = new DecisionNode(propertyCol);

        // cout << "\tfeatures.ValueCount(" << propertyCol << "): " << features.valueCount(propertyCol) << endl;

        for (size_t v=0; v<features.valueCount(propertyCol); v++)
        {
            Matrix subFeatures, subLabels;

            // if (features.attrValue(propertyCol, v) != "")
                // cout << "Value v(" << v << "): " << features.attrValue(propertyCol, v) << endl;
            // else
                // cout << "Value: " << v << endl;

            // cout << "features: " << features.toString() << endl;
            for (size_t r=0; r<features.rows(); r++)
            {
                if (features[r][propertyCol] == v)
                {
                    subFeatures.copyPart(features, r, 0, 1, features.cols());
                    subLabels.copyPart(labels, r, 0, 1, 1);
                }
            }

            if (subLabels.rows() > 0)
            {
                // cout << "\tDOWN A LEVEL" << endl;
                root->addChild(v, induceTree(subFeatures, subLabels, properties));
                // cout << "\tUP A LEVEL" << endl;
            }
            else
            {
                // cout << "\tDOWN A LEVEL" << endl;
                // cout << "\tNo More Sublabels...New Leaf Node: " << labels.attrValue(0, labels.mostCommonValue(0)) << endl;
                root->addChild(v, new DecisionNode(0, labels.mostCommonValue(0)));
                // cout << "\tUP A LEVEL" << endl;
            }
        }

        // Assign the most common attribute index
        size_t mostCommonIdx = 0;
        double greatestProportion = 0.0;
        for (size_t v=0; v<features.valueCount(propertyCol); v++) 
        {
            double prop = (double)features.countInColumn(propertyCol, v) / (double)features.rows();

            if (prop > greatestProportion)
            {
                greatestProportion = prop;
                mostCommonIdx = v;
            }
        }
        root->mostCommonAttrIdx = mostCommonIdx;

        return root;
    }

    size_t chooseProperty(Matrix& features, Matrix& labels, vector<size_t>& properties)
    {
        if (properties.size() == 1)
            return properties[0];

        if (splittingMethod == "information")
        {
            size_t bestGainProperty = 0;
            double bestGain = 0.0;
            // Get attribute column that yields highest info gain
            for (size_t p=0; p<properties.size(); p++)
            {
                double gainValue = gain(features, labels, properties[p]);
                if (gainValue > bestGain)
                {
                    bestGain = gainValue;
                    bestGainProperty = properties[p];
                }
            }

            return bestGainProperty;
        }
        else if (splittingMethod == "accuracy")
        {
            size_t bestAccuracyProperty = 0;
            double bestAccuracy = 0.0;
            // Get attribute column that yields highest accuracy
            for (size_t p=0; p<properties.size(); p++)
            {
                double accuracyValue = accuracy(features, labels, properties[p]);
                if (accuracyValue > bestAccuracy)
                {
                    bestAccuracy = accuracyValue;
                    bestAccuracyProperty = properties[p];
                }
            }

            // cout << "Returning Accuracy Property: " << bestAccuracyProperty << endl;
            return bestAccuracyProperty;
        }
        else if (splittingMethod == "extended")
        {
            size_t bestAccuracyProperty = 0;
            double bestAccuracy = 0.0;
            // Get attribute column that yields highest accuracy
            for (size_t p=0; p<properties.size(); p++)
            {
                for (size_t p2=0; p2<properties.size(); p2++)
                {
                    if (p2 == p)
                        continue;

                    double accuracyValue = extendedAccuracy(features, labels, properties[p], properties[p2]);
                    if (accuracyValue > bestAccuracy)
                    {
                        bestAccuracy = accuracyValue;
                        bestAccuracyProperty = properties[p];
                    }
                }
            }

            // cout << "Returning Accuracy Property: " << bestAccuracyProperty << endl;
            return bestAccuracyProperty;
        }
        
        return 0;
    }

    double gain(Matrix& features, Matrix& labels, size_t attributeCol)
    {
        double gain = entropy(labels);
        double sum = 0.0;

        for (size_t v=0; v<features.valueCount(attributeCol); v++)
        {
            Matrix subset;
            for (size_t r=0; r<features.rows(); r++)
            {
                if (features[r][attributeCol] == v)
                    subset.copyPart(labels, r, 0, 1, 1);
            }

            double proportion = (double)subset.rows() / (double)labels.rows();
            double subset_entropy = entropy(subset);

            sum += proportion * subset_entropy;
        }

        return gain - sum;
    }

    double entropy(Matrix& labels)
    {
        double entropy = 0.0;

        if (labels.rows() == 0)
            return entropy;

        for (size_t i=0; i<labels.valueCount(0); i++)
        {
            double proportion = (double)labels.countInColumn(0, i) / (double)labels.rows();
            if (proportion == 0)
                continue;

            entropy += -1 * proportion * (log(proportion) / log(2));
        }

        return entropy;
    }

    double accuracy(Matrix& features, Matrix& labels, size_t attributeCol)
    {
        double sum = 0.0;

        for (size_t v=0; v<features.valueCount(attributeCol); v++)
        {
            Matrix subset;
            for (size_t r=0; r<features.rows(); r++)
            {
                if (features[r][attributeCol] == v)
                    subset.copyPart(labels, r, 0, 1, 1);
            }

            double proportion = (double)subset.rows() / (double)labels.rows();
            double subset_accuracy = bestWeCanGuess(subset);
            // cout << "Proportion: " << proportion << endl;
            sum += proportion * subset_accuracy;
        }

        return sum;
    }

    double extendedAccuracy(Matrix& features, Matrix& labels, size_t attributeCol, size_t secondAttributeCol)
    {
        double sum = 0.0;

        for (size_t v=0; v<features.valueCount(attributeCol); v++)
        {
            for (size_t v2=0; v2<features.valueCount(secondAttributeCol); v2++)
            {
                Matrix subset;
                for (size_t r=0; r<features.rows(); r++)
                {
                    if (features[r][attributeCol] == v && features[r][secondAttributeCol] == v2)
                        subset.copyPart(labels, r, 0, 1, 1);
                }

                double proportion = (double)subset.rows() / (double)labels.rows();
                double subset_accuracy = bestWeCanGuess(subset);
                // cout << "Proportion: " << proportion << endl;
                sum += proportion * subset_accuracy;
            }
        }

        return sum;
    }

    double bestWeCanGuess(Matrix& labels)
    {
        if (labels.rows() == 0)
            return 0.0;

        return (double)labels.countInColumn(0, labels.mostCommonValue(0)) / (double)labels.rows();
    }
};


#endif // DECISION_TREE_H
