#ifndef DECISION_NODE_H
#define DECISION_NODE_H

#include <map>

using namespace std;

class DecisionNode 
{
private:

    /**
     * If this node has children, then the attribute Idx tells the tree
     * which attribute to split on at this node.
     */
    size_t      attributeIdx;

    /**
     * If this node is a leaf node, the classIdx tells the tree which
     * class is predicted at this leaf.
     */
    size_t      classIdx;

    /**
     * This node can have some number of children
     */
    map<size_t, DecisionNode*> children;

public: 

    /**
     * Most common idx, used when an unknown value is encountered at this node
     */
    size_t      mostCommonAttrIdx;

public:

    DecisionNode(size_t attributeIdx, size_t classIdx=0)
    {
        this->attributeIdx = attributeIdx;
        this->classIdx = classIdx;

        children.clear();
    }

    ~DecisionNode()
    {
        map<size_t, DecisionNode*>::iterator it;
        for (it=children.begin(); it!=children.end(); ++it)
        {
            DecisionNode* n = it->second;
            if (n != NULL)
            {
                delete n;
                it->second = NULL;
            }
        }

        children.clear();
    }

    void addChild(size_t propertyValue, DecisionNode* newChild)
    {
        children[propertyValue] = newChild;
    }

    size_t predictClass(const vector<double>& features)
    {
        if (children.empty())
        {
            // cout << "\t\t\tLeaf Node, class: " << classIdx << endl;
            return classIdx;
        }

        double childIdx = features[attributeIdx];
        // cout << "\t\tRoot Node, attribute: " << attributeIdx << ", children size: " << children.size() << ", index: " << childIdx << endl;
        if (childIdx == UNKNOWN_VALUE)
        {
            childIdx = mostCommonAttrIdx;        
            // cout << "\t\t\t\tUnknown found, using " << childIdx << endl;
        }
        return children[(size_t)childIdx]->predictClass(features);
    }

};

#endif

