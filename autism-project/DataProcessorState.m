
x = 2;
i = 0;

prev = dem_all(x,1);

fid = fopen('California.arff','w');
[r,c] = size(dem_all)
%place, hand, sex, age, VIQ, PIQ, FIQ
fprintf(fid,'@RELATION Data\n\n');
fprintf(fid,'@ATTRIBUTE PlaceOfRetrieval NUMERIC\n');
fprintf(fid,'@ATTRIBUTE Handedness {R,L,Ambi,Mixed}\n');
fprintf(fid,'@ATTRIBUTE Sex {M,F}\n');
fprintf(fid,'@ATTRIBUTE Age NUMERIC\n');
fprintf(fid,'@ATTRIBUTE VIQ NUMERIC\n');
fprintf(fid,'@ATTRIBUTE PIQ NUMERIC\n');
fprintf(fid,'@ATTRIBUTE FIQ NUMERIC\n');
fprintf(fid,'@ATTRIBUTE class {Autism,Control}\n');
fprintf(fid,'@DATA\n\n');

while (x < r+1)
    
    if(strcmp(prev,dem_all(x,1)) == 0)
        i = i + 1;
        prev = dem_all(x,1);
        
    end
    
    
    if(strcmp(dem_all{x,1},'CALTECH') == 1)
        fprintf(fid,'%d, ',1);
        ParseData()
    elseif(strcmp(dem_all{x,1},'UCLA_1') == 1)
        fprintf(fid,'%d, ',1);
        ParseData()
    elseif(strcmp(dem_all{x,1},'UCLA_2') == 1)
         fprintf(fid,'%d, ',1);
         ParseData()
    elseif(strcmp(dem_all{x,1},'UCLA_2') == 1)
         fprintf(fid,'%d, ',1);
         ParseData()
    end
    

   % end
    x = x + 1;
end

fclose(fid);