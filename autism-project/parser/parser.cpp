#include <iostream>
#include <sstream>

#include "matrix.h"
#include "mat.h"

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;

#define SUBJECT_COUNT 650
#define INSERT_ROW_COUNT 30000

string getFileForSubject(int subjectNumber)
{
    stringstream sstr;
    sstr.clear();
    sstr << "/media/drautb/WD_PASSPORT_1TB/ABIDE Data/cor7266/sub_0";
    if (subjectNumber < 10)
        sstr << "00" << subjectNumber;
    else if (subjectNumber < 100)
        sstr << "0" << subjectNumber;
    else
        sstr << subjectNumber;

    sstr << ".mat";

    return sstr.str();
}

int main(int argc, char* argv[])
{
    /**
     * Some variables
     */
    string filename;

    int arrayCount = 0;
    char **arrayList = NULL;

    mxArray* dataMatrix = NULL;
    const mwSize* matrixDims = NULL;
    double* dataPtr = NULL;

    unsigned long offset = 0;

    /**
     * Database stuff
     */
    sql::Driver* driver;
    sql::Connection* conn;
    sql::Statement* stmt;

    /**
     * Start Looping
     */
    for (int subjectNumber=1; subjectNumber<=SUBJECT_COUNT; subjectNumber++)
    {
        cout << "Starting import for subject number " << subjectNumber << endl;

        filename = getFileForSubject(subjectNumber);

        MATFile* f = matOpen(filename.c_str(), "r");
        if (f == NULL) {
            cout << "\tFailed to open file: " << filename << ", skipping subject." << endl;
            continue;
        }

        cout << "\tSuccessfully Loaded File..." << endl;

        arrayCount = 0;
        arrayList = matGetDir(f, &arrayCount);
        if (arrayCount == 0 || arrayList == NULL)
        {
            cout << "\tNo arrays found in file, skipping subject." << endl;
            matClose(f);
            continue;
        }

        dataMatrix = matGetVariable(f, arrayList[0]);
        if (dataMatrix == NULL)
        {
            cout << "\tFailed to load data matrix: " << arrayList[0] << ", skipping subject." << endl;
            mxFree(arrayList);
            matClose(f);
            continue;
        }

        matrixDims = mxGetDimensions(dataMatrix);

        if (!mxIsDouble(dataMatrix))
        {
            cout << "\tData are not doubles, skipping subject." << endl;
            mxFree(arrayList);
            matClose(f);
            continue;
        }

        dataPtr = mxGetPr(dataMatrix);
        if (dataPtr == NULL)
        {
            cout << "\tFailed to get data ptr, skipping subject." << endl;
            mxFree(arrayList);
            matClose(f);
            continue;
        }

        cout << "\tSuccessfully Loaded Data Pointer..." << endl;

        driver = get_driver_instance();
        conn = driver->connect("tcp://seed.byu.edu:3306", "parser", "parser5660");
        conn->setSchema("ABIDE");
        
        stringstream query;
        query.clear();
        query << "INSERT INTO `connection`(`subject_id`, `roi`, `connected_roi`, `value`) VALUES";

        int rowCount = 0;

        for (mwSize row=0; row<matrixDims[0]-1; row++)
        {
            for (mwSize col=row+1; col<matrixDims[1]; col++)
            {
                offset = row * matrixDims[0] + col;

                if (rowCount > 0)
                    query << ",";

                query << "('" << subjectNumber << "', '" << (row + 1) << "', '" << (col+1) << "', '" << dataPtr[offset] << "')";

                rowCount++;
                if (rowCount >= INSERT_ROW_COUNT)
                {
                    query << ";";
                    // cout << "QUERY:\n" << query.str() << endl << endl;

                    stmt = conn->createStatement();
                    stmt->execute(query.str().c_str());
                    stmt->close();
                    delete stmt;

                    rowCount = 0;
                    query.clear();
                    query.str(string());
                    query << "INSERT INTO `connection`(`subject_id`, `roi`, `connected_roi`, `value`) VALUES";
                }
            }
        }
        
        delete conn;

        // GLOBAL CLEANUP
        mxFree(arrayList);
        mxDestroyArray(dataMatrix);
        matClose(f);

        arrayList = NULL;
        dataMatrix = NULL;
        matrixDims = NULL;
    }
   
    return 0;

}

