
x = 2;
i = 0;

prev = dem_all(x,1);

fid = fopen('USMtest1.arff','w');
[r,c] = size(dem_all)
%place, hand, sex, age, VIQ, PIQ, FIQ
fprintf(fid,'@RELATION Data\n\n');
fprintf(fid,'@ATTRIBUTE PlaceOfRetrieval NUMERIC\n');
fprintf(fid,'@ATTRIBUTE Handedness NUMERIC\n');
fprintf(fid,'@ATTRIBUTE Sex {M,F}\n');
fprintf(fid,'@ATTRIBUTE Age NUMERIC\n');
fprintf(fid,'@ATTRIBUTE VIQ NUMERIC\n');
fprintf(fid,'@ATTRIBUTE PIQ NUMERIC\n');
fprintf(fid,'@ATTRIBUTE FIQ NUMERIC\n');
fprintf(fid,'@ATTRIBUTE class {Autism,Control}\n');
fprintf(fid,'@DATA\n\n');

while (x < r+1)
    
    if(strcmp(prev,dem_all(x,1)) == 0)
        i = i + 1;
        prev = dem_all(x,1);
        
    end
    
    
    if(strcmp(dem_all{x,1},'USM') == 1)
    fprintf(fid,'%d, ',i);
  

    if(isnan(dem_num_other(x-1,6)) ~= 1)
        hand = '';
        if(dem_num_other(x-1,6) == 1)
            hand = 0;
        elseif(dem_num_other(x-1,6) == 2)
            hand = 1;
        elseif(dem_num_other(x-1,6) == 3)
            hand = 2;
        end
     
        fprintf(fid,'%d, ',hand);
    else
        hand = '?';
        fprintf(fid,'%c, ',hand);
    end

    sex = -1;
    if(isnan(dem_all{x,8}) ~= 1)
        sex = '';
        if(dem_all{x,8} == 1)
            sex = 'M';
        else
            sex = 'F';
        end
        fprintf(fid,'%s, ',sex);
    else
        sex = '?';
        fprintf(fid,'%c, ',sex);
    end
    
    age = -1;
    if(isa(dem_all{x,7},'numeric') == 1)
        age = dem_all{x,7};
        fprintf(fid,'%f, ',age);
    else
        age = '?';
        fprintf(fid,'%c, ',age);
    end
    
    VIQ = -1;
    if(isnan(dem_all{x,12}) ~= 1)
        VIQ = dem_all{x,12};
        fprintf(fid,'%d, ',VIQ);
    else
        VIQ = '?';
        fprintf(fid,'%c, ',VIQ);
    end
    
    
    PIQ = -1;
    if(isnan(dem_all{x,13}) ~= 1)
        PIQ = dem_all{x,13};
        fprintf(fid,'%d, ',PIQ);
    else
        PIQ = '?';
        fprintf(fid,'%c, ',PIQ);
    end
    
    FIQ = -1;
    if(isnan(dem_all{x,11}) ~= 1)
        FIQ = dem_all{x,11};
        fprintf(fid,'%f, ',FIQ);
    else
        FIQ = '?';
        fprintf(fid,'%c, ',FIQ);
    end
    
    Cond = -1;
    if(isnan(dem_all{x,5}) ~= 1)
        if(dem_all{x,5} == 1)
            fprintf(fid,'Autism');
        else
            fprintf(fid,'Control');
        end
        Cond = dem_all{x,5};
        
    else
        Cond = '?';
        fprintf(fid,'%c',Cond);
    end
    
    
   % a = [i , sex, hand, dem_all(x,7), dem_all(x,12), dem_all(x,13), dem_all(x,11)];
   % fprintf(fid,'%f\n',i);
    fprintf(fid,'\n');
    end
    x = x + 1;
end

fclose(fid);