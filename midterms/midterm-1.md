<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

Midterm 1
=========

Ben Draut / CS 470 / Oct. 11, 2013

**1) True of False**

* True
* False
* False
* True
* False
* False
* True
* True

**2.** 

Overfitting means that a learning algorithm improves its accuracy on the training data while lowering accuracy on any new dataset. The model fits the training set too tightly, and can't generalize. It can be detected by testing a model against multiple training sets. ID3 _____

**3.**

NB assumes that the values of interest are governed by a probability distribution, and that all attributes are conditionally independent, given the class. Zero probabilities can be avoided by just replacing them with extremely small values.

**4.**

* **a.** My conclusion would be that the number of obese people tripled from 1950 to 200.

* **b.** My conclusion would be that the number of obese people increased by 20% or so.

* **c.** My conclusions were not the same. This is because they scaled the graphs differently in each graph. 

* **d.** Bar Chart:

<img src="../../Diagrams/graph.png" style="width:400px;"/>

* **e.** 

Lie factor of first: $\frac{\frac{1.5-0.5}{0.5}}{\frac{150-100}{100}} = 4$

Lie factor of second: $\frac{\frac{0.7-0.5}{0.5}}{\frac{150-100}{100}} = 0.8$

Neither were correct. (Between 0.95 and 1.05)

* **f.** Two principles of good visualization are proper scale, and consistency.

**5.**

* **a.** No, correlation does not imply causation.

* **b.** Analyze the data to see if the games that you lost on Mondays were games from which Carlos was absent.

* **c.** A confounding or lurking variable.

* **d.** They are often embedded more deeply in the data. Often they're not obvious enough for humans to observe a pattern.

**6.**

Proportion of Variance Covered: $\frac{2.0 + 1.4 + 0.6}{1.4 + 0.45 + 2.0 + 0.55 + 0.6} = 0.80$ or 80%

This tells me that the first 3 principal components do a pretty good job, but there is still a fair amount of variance to be covered. PCA doesn't help too much in this case because all attributes seem important.

**7.**

Machines are great at making predictions when everything can be quantified somehow, but we still have to make decisions that cannot be quantified. We cannot quantify the value of one life over another, nor is every situation the same. Machines are totally incapable of making decisions that have any sory of spiritual element as well. As humans, we sometimes make decisions based on feelings, impressions, hunches, intuition, etc, while Machines cannot do this. There are some things in life that simply are not deterministic or predictable. In situations like this, a human must decide.

**8.**

**9.**

* **a.** Eager

* **b.** The set of all target values. It generates models that only predict one class.

* **c.** Predict the class of $E_n$

* **d.** 

* **e.**

**10.**

* **a.**