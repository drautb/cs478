<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<style type="text/css">
body {
    /*font-family: "Arial";*/
    font-size: 10pt;
}
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 0.4em 0.6em;
}
</style>

ID3: Decision Tree Learning
===========================

Ben Draut / CS 478 / September 18, 2013

_In the diagrams, all attributes have been abbreviated to the acronym form to save space. In the Iris diagrams, numbers correspond to buckets that continuous values have been discretized into. In the Voting diagrams, no is always on the left, yes is always on the right._

<!-- **_Directions:_**

* Correctly implement the ID3 decision learning tree algorithm. (Note: you may wish to use a simple data set, like Lenses, that you can check by hand, to test your algorithm and make sure that it is working correctly; you should be able to get about 68% predictive accuracy on lenses with cross-validation). Your implementation must support the following.
    * An option for choosing the splitting criterion: information gain or accuracy.
    * Some mechanism to handle continuous-valued attributes.
> Using Discretize Filter in C++ Toolkit.
    * Some mechanism to handle unknown (or missing) attribute values.
> Using the most common value at the node to replace the missing value.
* Use your ID3 algorithm on the Iris problem.
    * Induce a decision tree using the entire dataset with information gain as the splitting criterion. Give a visual representation of the tree.
    * Induce a decision tree using the entire dataset with accuracy as the splitting criterion. Give a visual representation of the tree. Compare this tree with the one obtained with information gain as the splitting criterion.
    * Evaluate predictive accuracy using 10-fold cross-validation for information gain and accuracy. Compare the results.
* Repeat the experiment with the Voting problem.
    * Describe and justify the method you used to handle missing values.
* Extend your algorithm so that, when accuracy is the splitting criterion, it may use up to 2 conditions in the tests at each node (e.g., attrX = Vx and attrY = Vy). You may choose to make that an user-specified option.
    * Induce a decision tree using the entire dataset with this extended algorithm for both the Iris problem and the Voting problem. Give a visual representation of the trees and compare them with those obtained above.
    * Explain why it may be necessary to thus extend the decision tree learning algorithm when using accuracy as the splitting criterion (and why the extension is of little value when information gain is the splitting criterion). -->

<!-- ## Lenses Dataset (Testing)

Command:

    ./MLSystemManagerDbg -L decisiontree -A ../../../datasets/lenses.arff -E cross 10 -R 100

Output:

    Splitting Criteria: information
    Dataset name: ../../../datasets/lenses.arff
    Number of instances (rows): 24
    Number of attributes (cols): 5
    Learning algorithm: decisiontree
    Evaluation method: cross
    ...
    Mean predictive accuracy: 0.75 -->

## Iris Dataset

**Induce a decision tree using the entire dataset with information gain as the splitting criterion. Give a visual representation of the tree.**

    ./MLSystemManagerDbg -L decisiontree -A ../../../datasets/iris.arff -E training -D -R 100

<img src="../../Images/Iris-Information-Gain.png" style="width: 400px; margin: 0px 110px;"/>

**Induce a decision tree using the entire dataset with accuracy as the splitting criterion. Give a visual representation of the tree.**

    ./MLSystemManagerDbg -L decisiontree -A ../../../datasets/iris.arff -E training -D -R 100 -S accuracy

<img src="../../Images/Iris-Accuracy.png" style="width: 400px; margin: 0px 110px;"/>

**Compare this tree with the one obtained with information gain as the splitting criterion.**

In this case, both trees are fairly similar. Both trees ended up with with the same number of leaf nodes and edges. The trees look nearly identical, however they split on different attributes in a couple places.

**Evaluate predictive accuracy using 10-fold cross-validation for information gain and accuracy. Compare the results.**

    Splitting Criteria: information                 Splitting Criteria: accuracy
    Rep: 0, Fold: 0, Accuracy: 0.933333             Rep: 0, Fold: 0, Accuracy: 0.933333
    Rep: 0, Fold: 1, Accuracy: 1                    Rep: 0, Fold: 1, Accuracy: 1
    Rep: 0, Fold: 2, Accuracy: 0.866667             Rep: 0, Fold: 2, Accuracy: 0.866667
    Rep: 0, Fold: 3, Accuracy: 0.933333             Rep: 0, Fold: 3, Accuracy: 0.933333
    Rep: 0, Fold: 4, Accuracy: 0.933333             Rep: 0, Fold: 4, Accuracy: 0.933333
    Rep: 0, Fold: 5, Accuracy: 0.933333             Rep: 0, Fold: 5, Accuracy: 0.933333
    Rep: 0, Fold: 6, Accuracy: 0.866667             Rep: 0, Fold: 6, Accuracy: 0.866667
    Rep: 0, Fold: 7, Accuracy: 0.866667             Rep: 0, Fold: 7, Accuracy: 0.866667
    Rep: 0, Fold: 8, Accuracy: 1                    Rep: 0, Fold: 8, Accuracy: 1
    Rep: 0, Fold: 9, Accuracy: 0.866667             Rep: 0, Fold: 9, Accuracy: 0.866667
    Mean predictive accuracy: 0.92                  Mean predictive accuracy: 0.92

Both splitting methods performed identically for this dataset. We observed previously that the generated models were extremely similar, so this is no surprise that there is no difference in their accuracy.

## Voting Dataset

**Induce a decision tree using the entire dataset with information gain as the splitting criterion. Give a visual representation of the tree.**

    ./MLSystemManagerDbg -L decisiontree -A ../../../datasets/voting.arff -E training -R 100

<img src="../../Images/Voting-1.png" alt="Voting 1" style="width: 400px; margin: 0px 110px;"/>

**Induce a decision tree using the entire dataset with accuracy as the splitting criterion. Give a visual representation of the tree.**

    ./MLSystemManagerDbg -L decisiontree -A ../../../datasets/voting.arff -E training -R 100 -S accuracy

![](../../Images/Voting-Accuracy.png)

**Compare this tree with the one obtained with information gain as the splitting criterion.**

The tree that was produced by the accuracy is much larger than the other. The accuracy tree had to go much deeper on several branches to reach the same conclusions as the information gain tree. This is an excellent example of how information gain splits on what it determines to be the most _informative_ attributes, thus preventing it from having to create as many branches.

**Evaluate predictive accuracy using 10-fold cross-validation for information gain and accuracy. Compare the results.**

    Splitting Criteria: information                 Splitting Criteria: accuracy
    Rep: 0, Fold: 0, Accuracy: 0.953488             Rep: 0, Fold: 0, Accuracy: 0.976744
    Rep: 0, Fold: 1, Accuracy: 0.976744             Rep: 0, Fold: 1, Accuracy: 1
    Rep: 0, Fold: 2, Accuracy: 0.953488             Rep: 0, Fold: 2, Accuracy: 0.930233
    Rep: 0, Fold: 3, Accuracy: 0.976744             Rep: 0, Fold: 3, Accuracy: 0.953488
    Rep: 0, Fold: 4, Accuracy: 0.883721             Rep: 0, Fold: 4, Accuracy: 0.906977
    Rep: 0, Fold: 5, Accuracy: 0.930233             Rep: 0, Fold: 5, Accuracy: 0.953488
    Rep: 0, Fold: 6, Accuracy: 0.976744             Rep: 0, Fold: 6, Accuracy: 0.976744
    Rep: 0, Fold: 7, Accuracy: 0.930233             Rep: 0, Fold: 7, Accuracy: 0.930233
    Rep: 0, Fold: 8, Accuracy: 0.976744             Rep: 0, Fold: 8, Accuracy: 0.860465
    Rep: 0, Fold: 9, Accuracy: 0.953488             Rep: 0, Fold: 9, Accuracy: 0.906977
    Mean predictive accuracy: 0.951163              Mean predictive accuracy: 0.939535

On average, information gain is more accurate than accuracy. Accuracy did manage to be 100% accurate on one of the folds, and as high as information gain on some others, but it's performance is inconsistent. Accuracy is very hit-and-miss, while information gain is much more reliable.

**Describe and justify the method you used to handle missing values.**

When traversing the tree to make a prediction, if an attribute is encountered that has an unknown value, it is replaced with the most common value for that attribute at that particular node in the tree. This method is based on the assumption that the most likely value for an unknown attribute is the same as the majority. Statistically, this has a higher probability of being correct.

## Extension

**Induce a decision tree using the entire dataset with this extended algorithm for both the Iris problem and the Voting problem. Give a visual representation of the trees and compare them with those obtained above.**

    ./MLSystemManagerDbg -L decisiontree -A ../../../datasets/iris.arff -E training -D -R 100 -S extended

<img src="../../Images/Iris-Extended-Accuracy.png" style="width: 400px; margin: 0px 110px;"/>

    ./MLSystemManagerDbg -L decisiontree -A ../../../datasets/voting.arff -E training -D -R 100 -S extended

![](../../Images/Voting-Extended-Accuracy.png)

**Comparison:** 

The extended accuracy trees are much deeper than those above. Iris was also quite a bit less accurate this way, only achieving 87%. Voting, on the other hand, did somewhat better, achieving 94.5%.

**Explain why it may be necessary to thus extend the decision tree learning algorithm when using accuracy as the splitting criterion (and why the extension is of little value when information gain is the splitting criterion).**

Extending the accuracy criterion in this way gives the algorithm more of a look into future, helping it to discern which properties will be most beneficial to split on in the long run. Information gain does this intrinsically, therefore the additional computation would be wasted.
