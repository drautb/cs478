Thanksgiving Homework
=====================

Ben Draut / CS 478 / Dec. 2, 2013

### Thoughtfully answer the following questions:

**Consider your life's experiences and list one thing you have learned by being told, one thing you have learned by analogy, and one thing you have learned by induction. In each case, give enough details to show how your learning took place. Give a few reasons why you are grateful for your ability to learn.**

**Told:** I think the majority of what I learned in the younger years of my life I learned by having it told to me. Specifically, I think of principles of the Gospel. Both my parents at home and my teachers at church constantly told me things I hadn't known before.

**Analogy:** The first thing I thought of that I learned by analogy was the French language. As a missionary in the MTC, I found it very helpful to discover analogies between the meanings of words and phrases in both languages that helped me remember the distinctions.

**Induction:** I thought of principles of math and programming that I have learned by induction, especially rules of logic. 

I am very grateful for the ability to learn for a couple of reasons. The two that I think of the most are that I enjoy the feeling of learning something new, and that I enjoy being able to help other people using the things I have learned.


**Consider all of the articles listed on our schedule for Wednesday 11/18 (on Privacy). Using these and your own experience, do you think that people's view of privacy has been changing over the years? If so, why and how, and do you feel the trend will continue? If not, why not?**

I think that people's view of privacy has been changing. I personally think that people are growing more accustomed to the idea that they're losing privacy. This question reminded me of something Dr. Knutson shared with us. He said humorously (perhaps quoting someone else, I can't remember for sure) that anything that happens before we're 12 or so is something we look at as just the way things have always been, anything that happens between the time that we're 12 and 35 is a marvelous technological advancement, and anything that happens after we're 35 is contrary to nature. I think that this applies to privacy. The rising generation has only ever known a digital world where privacy is a rarity. I don't think that it's something they expect the way that my parents did. 


**Think of the members of your group (for your group project). For each one, indicate their name and write one thing you are grateful for about them specifically. (This will not be shared with them by me; you may share it with them yourself if you feel so inclined).**

I'm in a group with Nozomu Okuda, Derek Carr, and Colby Bates. I'm really grateful for Nozomu's willingness to work. I can always count on him finishing the assignments he accepts. I'm grateful for Derek's friendly and easy-going nature. He helps prevent the rest of us from stressing out too much. I'm grateful for Colby's sense of humor.

