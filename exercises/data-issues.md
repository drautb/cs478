<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Data Issues Homework
====================

Ben Draut / CS 478 / October 7, 2013

**Directions:**

Thoughtfully answer the following questions.

**1) Using your knowledge of how ID3 selects its root node, describe how ID3 could be used to design a simple attribute/feature selection mechanism.**

Because ID3 selects the most informative attribute as the root node in each level of the tree, we could simply take the attributes that ID3 splits on earliest as the most informative of the set. 

**2) Suppose that a potential large customer C has placed your company BetterSoft in competition with another company GoodSoft to test your relative abilities to develop good software. The task is to design an algorithm to solve a class P of problems. Your company produces algorithm A, whilst the other company produces algorithm B. Both you and your competitor are asked to run your own batch of 350 tests and report how often your algorithm gave acceptable solutions (as defined by C). GoodSoft comes out on top with a score of 83% against only 78% for your algorithm. Just as C is about to award its lucrative contract to GoodSoft, you realize that the problems in class P are not all of the same complexity. In fact, it turns out that there are two clear levels of difficulty: simple and complex. You ask C to collect more detailed data from GoodSoft and yourself. The results, when complexity is factored in, are as follows.**
 

| Simple       |                | Complex        |              |
| ------------ | -------------- | -------------- | ------------ |
| Alg A        | Alg B          | Alg A          | Alg B        |
| 81 out of 87 | 234 out of 270 | 192 out of 263 | 55 out of 80 |
| 0.9310       | 0.8667         | 0.7300         | 0.6875       |

* May this additional information change C's decision as to which company to hire? If so,how?

    * I think so. Even though GoodSoft's algorithm may perform better on average, BetterSoft was able to gain additional insight into the data by discovering a distinction between classifications of problems. Furthermore, when they split the data on the attribute they found, (complexity) they were able to achieve better predictive accuracy than GoodSoft in each respective category. Understanding the data better should encourage C to go with BetterSoft.

* A variable like complexity above is known as a confounding (or lurking) variable, because it interacts with the calculated outcome in a way that may easily be overlooked, but may have an adverse effect on the conclusions reached. Briefly describe another situation based on the real world where confounding effects would be at play.

    * Any situation where there is a bias in the sample is particularly subject to lurking variables. For example, if I ask for volunteers to take a survey about whether or not they think that helping others contributes to happiness, the results would likely be skewed due to the nature of the individuals who volunteer to take the survey.

**3) Using Weka and the Waveform dataset, experiment with CFS (known as CfsSubsetEval in Weka) and PCA (known as PrincipalComponents in Weka). Compare the results obtained with each method. What is the main difference between these two methods?**

Depending on the kind of search, CFS would select around 15 attributes to use. It seemed to just try to find the attributes that were most informative, similar to what we did when splitting in ID3. PCA, on the other hand, was primarily influenced by the percent of variance that it had to cover. It would select far more attributes, proportional to the percent of the variance it had to cover.

