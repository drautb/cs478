<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
body {
    font-size: 10pt
}
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Association Rule Mining
=======================

Ben Draut / CS 478 / Nov 4, 2013

**Directions:**

In this assignment, you will make use of existing algorithms to build a solution to a frequent association rule mining problem. You may implement your solution in R or Weka, and/or write your own code for all or parts of the solution.

Complete and report on the following:

**Use the Apriori algorithm on the Binarized Lenses problem. Set minconf = 0.8. Start with minsup = 0.9 and report your results (i.e., number of rules found and sample rules) for decreasing values of minsupusing decrements of 0.05, down to 0.05 (i.e., this means you should run the algorithm 18 times). Summarize your findings. Run the original Lenses problem against ID3 in Weka (or your own implementation if you prefer). Compare the rules you obtained with Apriori with the tree (or rules) induced by ID3.**

| Minimum Support | Rules Found | 
| --------------- | ----------- |
| 0.9             | 0           |
| 0.85            | 0           |
| 0.8             | 0           |
| 0.75            | 0           |
| 0.7             | 0           |
| 0.65            | 0           |
| 0.6             | 0           |
| 0.55            | 0           |
| 0.5             | 2           |
| 0.45            | 2           |
| 0.4             | 2           |
| 0.35            | 26          |
| 0.3             | 28          |
| 0.25            | 41          |
| 0.2             | 53          |
| 0.15            | >100        |
| 0.1             | >100        |
| 0.05            | >100        |

Some example rules were:

* norm-tearp-rate=0 12 ==> no-lenses=1 12    conf:(1)
* young=1 8 ==> pre-presbyopic=0 8    conf:(1)
* young=1 8 ==> presbyopic=0 8    conf:(1)

The minimum support value has an interesting effect. Depending on the data, I imagine you have to do some work to find just the right ballpark for that value. Apriori found 0 rules in this dataset until the minimum support went down to about 0.5, but once it got lower than 0.4, there were too many rules to be useful.

Running ID3 on the same dataset had some interesting results. ID3 had the norm-tearp-rate and young at the highest levels of the tree, showing that they were most informative. In this case, ID3's most informative attributes lined up with the rules found using Apriori.


**Use your algorithm on the Mirror Symmetry problem. Run Apriori for various combinations of minsup and minconf values. Summarize your findings. This is an artificial problem. Each attribute represents a bit position in a string of 30 bits: Lmost, Lmost1, ..., Lmost14, Rmost14, Rmost13, ..., Rmost1, Rmost and the attribute Symm is 1 if the pattern is symmetric about its center, and 0 otherwise. Given this interpretation, do any of the rules discovered by your Apriori algorithm make sense?**

| Minimum Support | Rules Found | 
| --------------- | ----------- |
| 0.9             | 0           |
| 0.85            | 0           |
| 0.8             | 0           |
| 0.75            | 0           |
| 0.7             | 0           |
| 0.65            | 0           |
| 0.6             | 0           |
| 0.55            | 0           |
| 0.5             | 0           |
| 0.45            | 1           |
| 0.4             | 4           |
| 0.35            | 6           |
| 0.3             | 16          |
| 0.25            | >100        |
| 0.2             | >100        |
| 0.15            | >100        |
| 0.1             | >100        |
| 0.05            | >100        |

Some example rules were:

* Lmost7=0 47 ==> Rmost7=0 40    conf:(0.85)
* Rmost4=1 52 ==> Lmost4=1 43    conf:(0.83)
* Lmost14=0 60 ==> Rmost14=0 48    conf:(0.8)
* Rmost5=0 50 ==> Lmost5=0 40    conf:(0.8)

I observed similar results on this dataset. Nothing informative was found until the support was lowered below 0.5, but once the support threshold was lowered below 0.3, there were too many rules to be useful.

Knowing that interpretation does make the rules make a bit more sense. The rules tell me that certain bits are more likely to be symmetrical than others.





