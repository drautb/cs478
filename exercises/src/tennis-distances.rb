# This is a quick and dirty script to calculate distances between days 
# for the kNN algorithm on the incremental learning homework assignemnt.

require 'pp'

$days = [
  { :day => 1,  :outlook => "Sunny",    :temperature => "Hot",  :humidity => "High",   :wind => "Weak",   :play => "No" }, 
  { :day => 2,  :outlook => "Sunny",    :temperature => "Hot",  :humidity => "High",   :wind => "Strong", :play => "No" }, 
  { :day => 3,  :outlook => "Overcast", :temperature => "Hot",  :humidity => "High",   :wind => "Weak",   :play => "Yes" },
  { :day => 4,  :outlook => "Rainy",    :temperature => "Mild", :humidity => "High",   :wind => "Weak",   :play => "Yes" },
  { :day => 5,  :outlook => "Rainy",    :temperature => "Cool", :humidity => "Normal", :wind => "Weak",   :play => "Yes" },
  { :day => 6,  :outlook => "Rainy",    :temperature => "Cool", :humidity => "Normal", :wind => "Strong", :play => "No" }, 
  { :day => 7,  :outlook => "Overcast", :temperature => "Cool", :humidity => "Normal", :wind => "Strong", :play => "Yes" },
  { :day => 8,  :outlook => "Sunny",    :temperature => "Mild", :humidity => "High",   :wind => "Weak",   :play => "No" }, 
  { :day => 9,  :outlook => "Sunny",    :temperature => "Cool", :humidity => "Normal", :wind => "Weak",   :play => "Yes" },
  { :day => 10, :outlook => "Rainy",    :temperature => "Mild", :humidity => "Normal", :wind => "Weak",   :play => "Yes" },
  { :day => 11, :outlook => "Sunny",    :temperature => "Mild", :humidity => "Normal", :wind => "Strong", :play => "Yes" },
  { :day => 12, :outlook => "Overcast", :temperature => "Mild", :humidity => "High",   :wind => "Strong", :play => "Yes" },
  { :day => 13, :outlook => "Overcast", :temperature => "Hot",  :humidity => "Normal", :wind => "Weak",   :play => "Yes" },
  { :day => 14, :outlook => "Rainy",    :temperature => "Mild", :humidity => "High",   :wind => "Strong", :play => "No" }, 
]

$outlook = {
  "Rainy" => 0,
  "Overcast" => 1,
  "Sunny" => 2
}

$temperature = {
  "Cool" => 0,
  "Mild" => 1,
  "Hot" => 2
}

$humidity = {
  "Normal" => 0,
  "High" => 1
}

$wind = {
  "Weak" => 0,
  "Strong" => 1
}

def distance(d1, d2)
  distance = 0
  distance += ($outlook[d1[:outlook]] - $outlook[d2[:outlook]]).abs
  distance += ($temperature[d1[:temperature]] - $temperature[d2[:temperature]]).abs
  distance += ($humidity[d1[:humidity]] - $humidity[d2[:humidity]]).abs
  distance += ($wind[d1[:wind]] - $wind[d2[:wind]]).abs
  distance
end

def hr
  puts "|" + "-" * 74 + "|"
end

puts "Calculating Distances between tennis $days..."

puts "\n\n"
header = "|"
0.upto(14) do |n| 
  header += " #{n}"
  if n < 10
    header += "  |"
  else
    header += " |"
  end
end

hr
puts header
hr

$days.each do |d1|
  daynum = d1[:day]
  line = "| #{daynum}"
  if daynum < 10
    line += "  "
  else
    line += " "
  end

  $days.each do |d2|
    d = distance(d1, d2)
    line += "| " + d.to_s
    if d < 10
      line += "  "
    else
      line += " "
    end
  end
  line += "|"
  puts line
  hr
end


