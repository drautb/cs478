package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	// "math"
)

type Iris struct {
	id      int
	cluster int
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Wrong number of arguments!")
		os.Exit(1)
	}

	fmt.Println("Computing F-Measure...")

	// Set up the TC (Target Clusters)
	tc := make([]Iris, 150)
	for i := 1; i <= 150; i++ {
		if i <= 50 {
			tc[i-1] = Iris{i, 1}
		} else if i <= 100 {
			tc[i-1] = Iris{i, 2}
		} else {
			tc[i-1] = Iris{i, 3}
		}
	}

	// Get tbhe CC (Computed Clusters)
	ccClusters := strings.Fields(os.Args[1])
	cc := make([]Iris, 150)
	for i := 1; i <= 150; i++ {
		cNum, _ := strconv.Atoi(ccClusters[i-1])
		cc[i-1] = Iris{i, cNum}
	}

	// a: number of pairs of items that belong to the same cluster in both CC and TC
	a := 0.0

	// b: number of pairs of items that belong to different clusters in both CC and TC
	b := 0.0

	// c: number of pairs of items that belong to the same cluster in CC
	//    but different clusters in TCpost
	c := 0.0

	// d: number of pairs of items that belong to the same cluster in TC
	//    but different clusters in CC
	d := 0.0

	// Do some crazy math
	for p1 := 0; p1 < 149; p1++ {

		for p2 := p1 + 1; p2 < 150; p2++ {

			if tc[p1].cluster == tc[p2].cluster && cc[p1].cluster == cc[p2].cluster {
				// If p1 and p2 are in the same cluster in both sets, a++
				a++
			} else if tc[p1].cluster != tc[p2].cluster && cc[p1].cluster != cc[p2].cluster {
				b++
			} else if tc[p1].cluster != tc[p2].cluster && cc[p1].cluster == cc[p2].cluster {
				// If p1 and p2 are in the same cluster in CC, but different in TC, c++
				c++
			} else if tc[p1].cluster == tc[p2].cluster && cc[p1].cluster != cc[p2].cluster {
				// If p1 and p2 are in the same cluster in TC, but different in CC, d++
				d++
			} else {
				fmt.Println("Impossible situation encountered...")
			}
		}

	}

	p := a / (a + c)
	r := a / (a + d)
	f := (2 * p * r) / (p + r)

	fmt.Printf("A: %d\nB: %d\nC: %d\nD: %d\n", int(a), int(b), int(c), int(d))
	fmt.Printf("Total Pairs: %d\n", int(a)+int(b)+int(c)+int(d))

	fmt.Println("F-Measure: ", f)
}
