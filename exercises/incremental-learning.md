<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<!-- This spaces out the table columns that MD generates -->
<style media="screen" type="text/css">
body {
    /*font-family: "Arial";*/
    font-size: 10pt;
}
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 0.4em 0.6em;
}
</style>
<style media="print" type="text/css">
body {
    /*font-family: "Arial";*/
    font-size: 10pt;
}
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 0.4em 0.6em;
}
</style>

Incremental Learning
====================

Ben Draut / CS 478 / September 20, 2013

## Introduction

Consider the following simple dataset. The target attribute is PlayTennis, which has values Yes or No for different Saturday mornings, depending on several other attributes of those mornings.

## k-Nearest Neighbors

#### Design a simple distance metric for this space. Briefly justify your choice.

One simple distance metric for this space is simply the number of attributes that differ between two samples. Distance would be:

$D(d_1, d_2) = count(a)$ where _a_ is some attribute where $a_{d_1} \neq a_{d_2}$

This metric provides an intuitive measure of distance. The greater the number of differing attributes, the greater the distance. It could be further improved by providing a scale for each attribute. Two attributes in this dataset have three ordered possible nominal values. Take the following example

$Temperature_{d_1} = Cool$

$Temperature_{d_2} = Hot$

Rather than increasing the distance by one for this difference, we would increase it by two to account for the additonal value that separates these two days. Given that each attribute value has a numerical index:

$D(d_1, d_2) = \sum_{i=0}^{n} abs(idx(v_{i_1}) - idx(v_{i_2}))$

Where $idx(v_{i_k})$ is the numerical index of the value of attribute _i_ for day _k_, and _n_ is the number of attributes.

Intuitively, this metric seems suitable. More formally, it is also apparent that:

$D(d_n, d_n) = 0$

and

$D(d_i, d_j) \equiv D(d_j, d_i)$

Thus satisfying our requirements for kNN distance metrics.

#### Perform 7-fold cross-validation with k=3 for this dataset. Show your work (there should be 7 iterations with 7 corresponding predictions/accuracies on the held-out folds, and a final result).

**Distance Between Days**

| 0      | 1  | 2  | 3  | 4  | 5  | 6  | 7  | 8  | 9  | 10 | 11 | 12 | 13 | 14 |    | Play? |
|--------|----|----|----|----|----|----|----|----|----|----|----|----|----|----|    | ----- |
| **1**  | -  | 1  | 1  | 3  | 5  | 6  | 5  | 1  | 3  | 4  | 3  | 3  | 2  | 4  |    | No    |
| **2**  | 1  | -  | 2  | 4  | 6  | 5  | 4  | 2  | 4  | 5  | 2  | 2  | 3  | 3  |    | No    |
| **3**  | 1  | 2  | -  | 2  | 4  | 5  | 4  | 2  | 4  | 3  | 4  | 2  | 1  | 3  |    | Yes   |
| **4**  | 3  | 4  | 2  | -  | 2  | 3  | 4  | 2  | 4  | 1  | 4  | 2  | 3  | 1  |    | Yes   |
| **5**  | 5  | 6  | 4  | 2  | -  | 1  | 2  | 4  | 2  | 1  | 4  | 4  | 3  | 3  |    | Yes   |
| **6**  | 6  | 5  | 5  | 3  | 1  | -  | 1  | 5  | 3  | 2  | 3  | 3  | 4  | 2  |    | No    |
| **7**  | 5  | 4  | 4  | 4  | 2  | 1  | -  | 4  | 2  | 3  | 2  | 2  | 3  | 3  |    | Yes   |
| **8**  | 1  | 2  | 2  | 2  | 4  | 5  | 4  | -  | 2  | 3  | 2  | 2  | 3  | 3  |    | No    |
| **9**  | 3  | 4  | 4  | 4  | 2  | 3  | 2  | 2  | -  | 3  | 2  | 4  | 3  | 5  |    | Yes   |
| **10** | 4  | 5  | 3  | 1  | 1  | 2  | 3  | 3  | 3  | -  | 3  | 3  | 2  | 2  |    | Yes   |
| **11** | 3  | 2  | 4  | 4  | 4  | 3  | 2  | 2  | 2  | 3  | -  | 2  | 3  | 3  |    | Yes   |
| **12** | 3  | 2  | 2  | 2  | 4  | 3  | 2  | 2  | 4  | 3  | 2  | -  | 3  | 1  |    | Yes   |
| **13** | 2  | 3  | 1  | 3  | 3  | 4  | 3  | 3  | 3  | 2  | 3  | 3  | -  | 4  |    | Yes   |
| **14** | 4  | 3  | 3  | 1  | 3  | 2  | 3  | 3  | 5  | 2  | 3  | 1  | 4  | -  |    | No    |

* **1st Iteration, days 1-12**
    * Day 13 Nearest Neighbors: (1, No), (3, Yes), (10, Yes) -> Predict Day 13: Yes (Correct)
    * Day 14 Nearest Neighbors: (4, Yes), (6, No), (12, Yes) -> Predict Day 14: Yes (INCORRECT)
        * **Accuracy: 50%**

* **2nd Iteration, days 1-10, 13-14**
    * Day 11 Nearest Neighbors: (2, No), (7, Yes), (8, No) -> Predict Day 11: No (INCORRECT)
    * Day 12 Nearest Neighbors: (2, No), (3, Yes), (14, No) -> Predict Day 12: No (INCORRECT)
        * **Accuracy: 0%**

* **3rd Iteration, days 1-8, 11-14**
    * Day 9 Nearest Neighbors: (5, Yes), (7, Yes), (8, No) -> Predict Day 9: Yes (Correct)
    * Day 10 Nearest Neighbors: (4, Yes), (5, Yes), (6, No) -> Predict Day 10: Yes (Correct)
        * **Accuracy: 100%**

* **4th Iteration, days 1-6, 9-14**
    * Day 7 Nearest Neighbors: (5, Yes), (6, No), (9, Yes) -> Predict Day 7: Yes (Correct)
    * Day 8 Nearest Neighbors: (1, No), (2, No), (3, Yes) -> Predict Day 8: No (Correct)
        * **Accuracy: 100%**

* **5th Iteration, days 1-4, 7-14**
    * Day 5 Nearest Neighbors: (4, Yes), (7, Yes), (10, Yes) -> Predict Day 5: Yes (Correct)
    * Day 6 Nearest Neighbors: (7, Yes), (10, Yes), (14, No) -> Predict Day 6: Yes (INCORRECT)
        * **Accuracy: 50%**

* **6th Iteration, days 1-2, 5-14**
    * Day 3 Nearest Neighbors: (1, No), (2, No), (13, Yes) -> Predict Day 3: No (INCORRECT)
    * Day 4 Nearest Neighbors: (5, Yes), (10, Yes), (14, No) -> Predict Day 4: Yes (Correct)
        * **Accuracy: 50%**

* **7th Iteration, days 3-14**
    * Day 1 Nearest Neighbors: (3, Yes), (8, No), (13, Yes) -> Predict Day 1: Yes (INCORRECT)
    * Day 2 Nearest Neighbors: (3, Yes), (8, No), (11, Yes) -> Predict Day 2: Yes (INCORRECT)
        * **Accuracy: 0%**

**Mean Accuracy: 50%**


## Naive Bayes

#### Using the full dataset, induce the corresponding NB model. Show your result in the form of probability tables as we did in class.

**Probability Tables**

| Play Tennis | Yes  | No   |
| ----------- | ----:| ----:|
|             | 9    | 5    |
|             | 0.64 | 0.36 |

<br/>

| Outlook  | Yes  | No  |
| --------:| ----:| ---:|
| Rainy    | 0.33 | 0.4 |
| Overcast | 0.44 | 0.0 |
| Sunny    | 0.22 | 0.6 |

<br/>

| Temperature  | Yes  | No  |
| ------------:| ----:| ---:|
| Cool         | 0.33 | 0.2 |
| Mild         | 0.44 | 0.4 |
| Hot          | 0.22 | 0.4 |

<br/>

| Humidity | Yes  | No  |
| --------:| ----:| ---:|
| Normal   | 0.66 | 0.2 |
| High     | 0.33 | 0.8 |

<br/>

| Wind   | Yes  | No  |
| ------:| ----:| ---:|
| Weak   | 0.66 | 0.4 |
| Strong | 0.33 | 0.6 |


#### What would your model predict for the following two Saturday mornings? Show your work.

* [Oct 1, Overcast, Cool, High, Weak]
    * **Yes:** 0.64 * 0.44 * 0.33 * 0.33 * 0.66 = 0.02024
    * **No:** 0.36 * 0.0 * 0.2 * 0.8 * 0.4 = 0.0
        * Prediction: _Yes_

* [May 26, Sunny, Hot, Normal, Strong]
    * **Yes:** 0.64 * 0.22 * 0.22 * 0.66 * 0.33000 = 0.00675
    * **No:** 0.36 * 0.6 * 0.4 * 0.2 * 0.6 = 0.010368
        * Prediction: _No_


