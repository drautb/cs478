<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<style type="text/css">
body {
    font-size: 10pt;
}

table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    font-size: 9pt;
    border: 1px solid #ddd;
    padding: 4px 3px;
}

.img-container {
    width: 100%;
    text-align: center;
}

.img-container img {
    width: 200px;
}

</style>

Neural Network Homework
=======================

Ben Draut / CS 478 / September 30, 2013

**Directions:**

**A. Consider the following simple dataset.**

| A | B | T |
| - | - | - |
| 1 | 0 | 1 |
| 0 | 1 | 0 |

**T is the (binary) target attribute. Consider a 2-layer feedforward neural network with two input units (one for A and one for B), a single hidden unit, and one output unit (for T). Initialize all weights (there should be 3 of them) to 0.1. Assume a learning rate of 0.3. Using incremental weight updates, show the values of the weights after each of the first three training iterations. Show your results in the form of a table as we did in class.**

**Visual:**

<div class="img-container">
    <img src="../../Images/Neural-Network.png"/>
</div>

**Iteration Table:**

| Itr. # | A | B | WAH    | WBH    | Out H  | WHT    | Out T  | Target | δT     | δH     | ΔWHT   | ΔWAH     | ΔWBH   |
| ------ | - | - | ------:| ------:| ------:| ------:| ------:| ------:| ------:| ------:| ------:| --------:| ------:|
| 1      | 1 | 0 | 0.1    | 0.1    | 0.5250 | 0.1    | 0.5131 | 1      | 0.1216 | 0.0030 | 0.0192 | 0.0009   | 0.0    |
|        | 0 | 1 | 0.1009 | 0.1    | 0.5250 | 0.1192 | 0.5156 | 0      |-0.1288 |-0.0038 |-0.0203 | 0.0      |-0.0011 |
| 2      | 1 | 0 | 0.1009 | 0.0989 | 0.5252 | 0.0989 | 0.5130 | 1      | 0.1217 | 0.0030 | 0.0187 | 0.0009   | 0.0    |
|        | 0 | 1 | 0.1018 | 0.0989 | 0.5247 | 0.1176 | 0.5154 | 0      |-0.1287 |-0.0038 |-0.0203 | 0.0      |-0.0011 |
| 3      | 1 | 0 | 0.1018 | 0.0978 | 0.5254 | 0.0973 | 0.5128 | 1      | 0.1217 | 0.0030 | 0.0192 | 0.000886 | 0.0    |
|        | 0 | 1 | 0.1027 | 0.0978 | 0.5244 | 0.1165 | 0.5153 | 0      |-0.1287 |-0.0037 |-0.0202 | 0.0      |-0.0011 |

<br/>

**B. Assume that the units of a neural network are modified so they compute the squashing function tanh (instead of the sigmoid function). What is the resulting backpropagation weight update rule for the output layer? (Note, tanh’(x) = 1 – tanh^2(x)).**

When using the sigmoid function, the backpropagation weight update rule for an output unit _k_ is:

$\delta_k = o_k(1-o_k)(t_k-o_k)$

Where $o_k$ is the value of unit _k_, $t_k$ is the target value for _k_, and $o_k(1-o_k)$ is the derivative of the sigmoid function.

When _tanh_ is used for the squashing function, the resulting rule is:

$(1-tanh^2(o_k))(t_k-o_k)$


