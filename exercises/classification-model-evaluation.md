<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
body {
    font-size: 10pt;
}
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Classification Model Evaluation
===============================

Ben Draut / CS 478 / October 12, 2013

Assume that two individuals offer to sell you their predictive models M1 and M2. The confusion matrices produced by each model are as follows.

Performance of M1

|                | Predicted True | Predicted False |
| -------------- | -------------- | --------------- |
| Actually True  | 5              | 95              |
| Actually False | 10             | 90              |

Performance of M2

|                | Predicted True | Predicted False |
| -------------- | -------------- | --------------- |
| Actually True  | 85             | 15              |
| Actually False | 95             | 5               |

1) What is the accuracy of each model?

$Accuracy_{M1} = \frac{5+90}{5+95+10+90} = 0.475$

$Accuracy_{M2} = \frac{85+5}{85+15+95+5} = 0.450$


2) Assuming that precision is of paramount importance in your application, which of the two models would you buy? Why?

$Precision_{M1} = \frac{5}{5 + 10} = 0.3333$

$Precision_{M2} = \frac{85}{85+95} = 0.4722$

I would buy M2, because it has higher precision.


3) Assuming that the cost of labeling as True something that is actually False far exceeds the cost of labeling as False something that is actually True, which of the two models would you buy? Why?

I would buy M1. Though M1 lacks precision, it predicts false positives (Label True when actually False) far less frequently than M2, thus it would reduce our cost.
