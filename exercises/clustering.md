<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
body {
    font-size: 10pt;
}
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Clustering
==========

Ben Draut / CS 478 / Oct. 25, 2013

**Run the k-means algorithm (kmeans) on the iris dataset (iris was loaded above when you loaded the R Datasets Package). Of course, iris has a target attribute. It must be excluded from the clustering. The simplest way to do this is to create a copy of iris consisting of only the first 4 attributes. This can be accomplished with the command: 'iris_copy <- subset(iris, select=c(1:4))'. You can then run kmeans on iris_copy.**
    
* Run k-means for k=2,3,4,5,7,9,11.
* For each value of k, report the size of the clusters and the F-measure (see this for details). Both size and cluster assignments are available in variables computed during k-means (see the documentation in the R Stats Package under k-means). You will need the target values from the original iris dataset to compute the F-score. You may write a small program in R to do this or export the data and compute elsewhere.
* Report the value of k that produces the highest F-score.
* Comment on anything interesting about your experiment.

_K-Means Analysis_

| k  | Cluster Sizes                        | F-Measure |
| -- | ------------------------------------ | --------- |
| 2  | 53, 97                               | 0.72798   |
| 3  | 38, 50, 62                           | 0.82066   |
| 4  | 50, 28, 32, 40                       | 0.75099   |
| 5  | 28, 22, 28, 45, 27                   | 0.59803   |
| 7  | 22, 7, 21, 28, 29, 21, 22            | 0.60164   |
| 9  | 12, 21, 15, 7, 5, 33, 24, 16, 17     | 0.44958   |
| 11 | 5, 24, 32, 13, 4, 5, 40, 8, 10, 4, 5 | 0.49114   |

The highest f-measure was achieved with _k_ being 3, which was what I expected since there were actually three different classifications in the data. It was interesting to see the f-measure fall as _k_ got further away from 3. It was also interesting to watch the way that _a_, _b_, _c_, and _d_ changed (when calculating the f-measure) as _k_ changed. _a_ kept shrkinking, while _d_ got bigger and bigger, which matches exactly what we would expect.


**Run the hierarchical clustering algorithm (hclust) on the iris dataset using complete link for the distance. Be mindful that hclust requires a distance matrix rather than a set of points as input. You can easily transform a set of points into its equivalent distance matrix using the dist() function. From iris_copy, you could thus construct iris_dist with the command: 'iris_dist <- dist(iris_copy)'. You can then run hclust on iris_dist.**
    
* Display and include in your report the result of hierarchical clustering. Use the function plot() to graph the dendrogram. You can copy the result to a postscript file using the following commands: 'postscript("nameoffile.eps")', then 'plot(resultofhac)', and finally 'dev.off()'.

![](../../Images/Dendrogram.png)

* By looking at the display or using the values of clustering heights, select a threshold at which you feel the clustering would be optimal and justify your choice. (In principle, we would do this by computing some quality measure during the clustering process, but for simplicity, we are just eyeballing here).

I would guess somewhere around 3 based on the dendrogram. At 2, there are still too many clusters that are very close, and at 4, they've grown too far apart. Around 3 seems to be the right medium where you've grouped enough points together to be useful, but not so many that it's stretching.

* How does the corresponding number of clusters compare with that obtained with k-means above?

The dendrogram matches up with k-means. 3 clusters seems to be optimal in both cases. Again, this seems to be expected since we know that there are actually 3 classes in the training data.

**Consider the swiss dataset. Use clustering, either k-means or hierarchical clustering (whichever seems to make most sense), to produce a list of the Swiss cities predominantly protestant and those predominantly catholic. You may produce a graph or simply a list.**

> The Swiss dataset doesn't contain any explicit data about who is Protestant, only Catholic. For this assignment, I'm assuming that all individuals are one or the other. (% Protestant = 1 - % Catholic)

I decided to just use k-means with 2 centers, since ideally I want two clusters: predominantly catholic, and predominantly protestant. I also only used the Catholic data column, since the others may or may not be related. 

    library(stats)
    library(datasets)
    swiss_copy <- subset(swiss, select=c(5:5))
    kmeans(swiss_copy, 2)

This yielded two clusters of sizes 17 and 30. Cluster 1 had a mean Catholic percentage of 94%, and cluster 2 had a mean Catholic percentage of 11%.

| Cluster 1 (Predominantly Catholic) | Cluster 2 (Predominantly Protestant) |
| ---------------------------------- | ------------------------------------ |
|     Delemont                       |   Courtelary                         |
| Franches-Mnt                       |      Moutier                         |
|   Porrentruy                       |   Neuveville                         |
|        Broye                       |        Aigle                         |
|        Glane                       |      Aubonne                         |
|      Gruyere                       |     Avenches                         |
|       Sarine                       |     Cossonay                         |
|      Veveyse                       |    Echallens                         |
|      Conthey                       |     Grandson                         |
|    Entremont                       |     Lausanne                         |
|       Herens                       |    La Vallee                         |
|     Martigwy                       |       Lavaux                         |
|      Monthey                       |       Morges                         |
|   St Maurice                       |       Moudon                         |
|       Sierre                       |        Nyone                         |
|         Sion                       |         Orbe                         |
|  Rive Gauche                       |         Oron                         |
|                                    |      Payerne                         |
|                                    | Paysd'enhaut                         |
|                                    |        Rolle                         |
|                                    |        Vevey                         |
|                                    |      Yverdon                         |
|                                    |       Boudry                         |
|                                    | La Chauxdfnd                         |
|                                    |     Le Locle                         |
|                                    |    Neuchatel                         |
|                                    |   Val de Ruz                         |
|                                    | ValdeTravers                         |
|                                    | V. De Geneve                         |
|                                    |  Rive Droite                         |


