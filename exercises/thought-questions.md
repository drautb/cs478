Thought Questions
=================

Ben Draut / CS 478-1 / September 7, 2013

### Questions

**Directions:** For each of the following applications, decide whether ML/DM would offer a viable solution and briefly justify you decision.

**1) Predicting whether or not a particular chemical compound is carcinogenic.**

I believe ML/DM could provide a viable solution to this problem. Given data regarding chemical structure and properties of 
each material, ML/DM algorithms would be able to find correlations, if any exist, between the chemical properties of a 
material, and whether it causes cancer.

**2) Determining which African-American applicants should be extended a home loan.**

Definitely, there were several ML/DM success stories where different loan agencies used ML/DM to predict whether or not
certain individuals or groups would have greater difficulty repaying a loan.

**3) Discovering what grocery items Wal-Mart customers tend to buy together.**

Absolutely. Given a dataset that contains different purchases with all the items included, ML/DM algorithms could easily 
discover which items were most frequently purchased together. There is more than enough data there for this to work.

**4) Grouping Wells Fargo customers by socio-demographic attributes and banking habits.**

I would say so. This seems to be a bit more complicated scenario, but again, there were several success stories about 
banks that used ML/DM to classify their customers in order to determine which ones were most likely to leave, so that 
they could market to them.

**5) Predicting tomorrow's value of Microsoft's stock.**

This one I'm not sure about. I think that ML/DM could offer a solution to this, but it would require a _massive_ 
dataset. The data would have to include everything from information about the season, to information about world
politics. Given enough data, I think it could work, but thinking about how much data it would require, I would 
say no, ML/DM doesn't offer a _viable_ solution at this time. There is too little bias as well, the domain is too broad.

**6) Predicting whether a Netflix subscriber will rent a particular new release.**

Yes. I'll bet they already do this. Videos are already categorized in several ways: rating, genre, length, etc. ML/DM 
lends itself perfectly to this. The data could also include customer information, such as geographic location, that 
could be used to further increase the certainty of predictions.

**7) Sorting a list of number in ascending order.**

Not so much...ML/DM is about finding a hypothesis that approaches a target function so that we can predict outcomes 
with some degree of certainty about situations we haven't seen before. When it comes to sorting a list of numbers,
we already know the target function. Given a list of numbers never before seen, sorting is a trivial operation.

**8) Identifying terrorists.**

Definitely. There was a success story about the FBI using ML/DM to analyze patterns in where people lived, shopped,
what they bought, where they traveled, etc. in order to predict if they were terrorists or not.

### Application

One application of ML/DM that I've considered has to do with the Church's efforts to use social media. ML/DM lends itself well to detecting patterns in massive datasets, so it could be used, for example, to see if there is a correlation between geographic areas that have a high number of re-tweets of Mormon.org tweets, and areas that have large numbers of referrals or new investigators.




