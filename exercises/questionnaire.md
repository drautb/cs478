Group Project Questionnaire
===========================

Ben Draut / CS 478 / December 9, 2013

Group: Autism Group 2 (Colby Bates, Derek Carr, Nozomu Okuda)

### Personal Views of Group

**Did your group work well together? Why/why not?**

I feel like we worked pretty well together, I think we just had some
communication difficulties sometimes. I feel that this was primarily due to
varying levels of understanding about the previous research that had been done.

I spent a lot of time reading the initial study paper and trying to understand
exactly what had been done before. The rest of our group admitted that they had
not, which frustrated me. I felt like we needed to have a good understanding of
the paper in order to take a follow-up step, so my ideas were centered around
this. The rest of the group suggested a couple of different ideas that involved
either discarding the previous research and taking our own direction, or using
the previous research in a way that didn't really make sense. Because of this,
we had a hard time agreeing on concrete goals, which limited the progress we
could make.

**Was the work allocated fairly?**

I believe the work that we did was allocated fairly.

**Was any work wasted? Why?**

I spent a significant amount of time earlier in the semester setting up a
database for us and writing a program to import the data, but we ended up just
using the supercomputer to do everything instead, so that work was wasted. I
blame myself for that though, we ought to have planned better.

**Did everyone in the group make an even effort or were there outstanding exceptions?**

When there were concrete things to be done, I feel that everyone made an even effort. However, I feel like there was a significant lack of effort on the others' part to understand the data and initial research that had been done.


### Personal View of Self

**What was the most interesting you learned from doing this project?**

This importance of having a well understood starting point. I feel like we were
all going different directions and had a hard time establishing goals.

**Did you contribute your full share to the project? More? Less? Why?**

I feel that I contributed my full share to the project, though I too was lacking. I assumed the role of group leader, but I think I neglected to do a good job of keeping the group on the same page.

**Approximately how many hours did you spend on the project?**

I spent approximately 40-45 hours on the project. (About 10 of those hours were
setting up the database that we didn't use though.)


### Suggested Grades

**If You Had To Award Grades (between 0 And 100) To Your Colleagues And Yourself On The Project, What Would They Be?**

* Myself: 85
* Colby Bates: 85
* Derek Carr: 85
* Nozomu Okuda: 85


### Other Comments

None.

