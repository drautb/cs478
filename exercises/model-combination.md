<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Model Combination
=================

Ben Draut / CS 478 / Oct. 21, 2013

**Show that stacking N (base) algorithms (where the stacking considers only the predictions of the base learners) does not generally produce the same result as an ensemble of the same N (base) algorithms combined by majority voting. You need not write a formal proof; make your argument in prose (with diagrams as needed).**

When stacking N algorithms, another algorithm is introduced as a meta-learner to try to learn which algorithms perform best in certain situations. This will be different than the result of majority voting on the same N algorithms, thus yielding a different result overall.


**Using Weka and a couple of datasets of your choice, compare the 10-fold cross-validation accuracies of the following algorithms:**

* Decision tree (use Weka's J48)
* k-NN (use Weka's IBk algorithm and set k=3)
* Naive Bayes
* Backpropagation learning (Use Weka's MultilayerPerceptron)
* An ensemble of the above combined with majority vote (Use Weka's Vote algorithm under "meta"; make sure to set it up for majority voting)
* A stacking of the above with each one of them used in turn as the meta learner (you will get 4 different accuracies here)


| Data Set | Decision Tree | k-NN (k=3) | Naive Bayes | Backprop | Ensemble Maj. Vote | 
| -------- | ------------- | ---------- | ----------- | -------- | ------------------ |
| Lenses   | 83.333%       | 79.167%    | 70.833%     | 70.833%  | 75.000%            |
| Iris     | 96.000%       | 95.333%    | 96.000%     | 97.333%  | 95.333%            |


**Stacking J48, k-NN, NB, and BP with each as the meta learner**

| Data Set | Decision Tree | k-NN (k=3) | Naive Bayes | Backprop |
| -------- | ------------- | ---------- | ----------- | -------- |
| Lenses   | 70.833%       | 75.000%    | 75.000%     | 70.833%  |
| Iris     | 92.667%       | 93.333%    | 95.333%     | 95.333%  |

These results were interesting, they weren't quite what I expected. For the most part, each algorithm did better individually then they did when combined. Especially on the Iris dataset. Every single combination model was no better than the worst accuracy of the individual algorithms.

