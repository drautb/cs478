<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Linear Regression and SVM
=========================

Ben Draut / CS 478 / Nov. 11, 2013

**Assume that logistic regression applied to a set of supercomputers produces the following simple model: log odds(solution in less than 5 min) = -14.0 + 0.25 NumCPUs. Answer the following questions (all answers should be in terms of e, where e is the inverse function of log; simplify as needed)**

**1) What are the odds that a 64 CPU supercomputer will find a solution in less than 5 min?**

$logit(P) = -14.0 + 0.25 * 64 = 2.0$

$P = \frac{1}{1 + e^{-2.0}}$

**2) How much better are the odds for a 80 CPU supercomputer (i.e., give the odds ratio)?**

$logit(P) = -14.0 + 0.25 * 80 = 6.0$

$P = \frac{1}{1 + e^{-6.0}}$

Odds ratio:

$\frac{odds(80)}{odds(64)} = \frac{e^{6.0}}{e^{2.0}} = \frac{403.429}{7.389} = 54.598$

The 80 CPU supercomputer's odds are $54.598$ times (or $5460\%$) better than the 60 CPU supercomputer.

**3) What is the probability that a 46 CPU supercomputer will find a solution in less than 5 min?**

$logit(P) = -14.0 + 0.25 * 46 = -2.5$

$P = \frac{1}{1 + e^{2.5}}$

**4) What size supercomputer would you need to have a probability 0.73 (=e/(1+e)) of finding a solution in less than 5 minutes?**

$\frac{1}{1 + e^{14-0.25n}} = 0.73$

$1.37 = 1 + e^{14-0.25n}$

$ln(0.37) = ln(e^{14-0.25n})$

$-0.995 = 14 - 0.25n$

$n = 59.98$

You would need supercomputer with at least 60 CPUs in order to achieve this probability.


**Consider the following transformation (from 3D to 10D space): $f(x) = f(x_1, x_2, x_3) = (1, \sqrt{2x_1}, \sqrt{2x_2}, \sqrt{2x_3}, {x_1}^2, {x_2}^2, {x_3}^2, \sqrt{2x_1x_2}, \sqrt{2x_1x_3}, \sqrt{2x_2x_3})$ **

**1) Show that $K(x, y) = < f(x), f(y) >$ is a kernel function by reducing $K(x, y)$ to $(1 + < x, y >)^2$ (recall that $< x, y >$ is the inner product, i.e., the sum of products of pairwise coordinates).**

**2) Using the SMO implementation of support vector machines in Weka, with the polynomial kernel, verify whether the above transformation is useful in learning the sqrt dataset. You should make sure that the polynomial kernel's degree in SMO is set to 2 (the default is 1) and that the useLowerOrder parameter is set to True. Compare SMO with, for example, VotedPerceptron, which is a simple linear model learner. Show the values of accuracy for both and discuss your findings. You may want to visualize the data in Weka.**

| Method | Accuracy |
| ------ | -------- |
| Voted Perception (1 iteration) | 50% |
| Voted Perceptron (100 iterations) | 63% |
| SMO | 61% |

Voted Perceptron only achieved 50% accuracy initially, but increasing the number of iterations to 100 improved the accuracy to over 60%. SMO on the other hand, only achieved 61% accuracy initially. On this particular dataset with the chosen kernel, accuracy was not significantly improved.


