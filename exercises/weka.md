<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

WEKA Exercise
=============

Ben Draut / CS 478-1 / Septempber 13, 2013

## Using the Cardiology dataset, do the following:

1) Build a neural network (using the Multilayer Perceptron/Backpropagation algorithm) that predicts whether a patient has a heart condition. Record the 10-fold cross-validation accuracy of your model as A1. Cross-validation is a form of model validation where a dataset is split into folds, and the learning algorithm is trained on all but one fold and tested on the remaining fold; the process is repeated for each fold and accuracy is averaged over all folds.

    MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a

* $A_1 = 81.8482\%$

2) Create a new attribute coarseBloodPressure, with values: 1 if blood pressure is less than or equal to 120, 2 if blood pressure is greater than 120 but less than or equal to 150, and 3 if blood pressure is greater than 150.

    Copy -R 4
    MathExpression -unset-class-temporarily -E "ifelse(A>120, ifelse(A>150, 3, 2), 1)" -R 1-14
    NumericToNominal -R 15

    Label   Count
    1       97 
    2       172
    3       34 

3) Build a neural network (using the same algorithm as above) that predicts whether a patient has a heart condition, using the new attributecoarseBloodPressure instead of the original blood pressure. Record the 10-fold cross-validation accuracy of your model as A2.

    Remove -R 4
    MultilayerPerceptron -L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a

* $A_2 = 81.1881\%$

4) Compare A1 and A2. Any comments?

* It was just slightly less accurate when using the nominal values for blood pressure as opposed to the actual numeric values. It doesn't seem to have a large effect.

5) Remove all records whose value of the attribute resting ecg is Abnormal.

    RemoveWithValues -S 0.0 -C 6 -L 3

6) Construct a decision tree that predicts whether a patient has a heart condition, given the attributes age, sex, chest pain type, coarseBloodPressure,angina, peak, and slope. Insert the confusion matrix obtained with 10-fold cross-validation in your report.

    Remove -R 4-7,11-12
    LMT -I -1 -M 15 -W 0.0

$Accuracy = 79.5987\%$

    === Confusion Matrix ===

    a    b       <-- classified as
    103  32  |   a = Sick
    29   135 |   b = Healthy

## Using the CPU dataset, do the following:

1) Cluster the data (using the simple k-Means algorithm, with k=3) and report on the nature and composition of the extracted clusters.

    SimpleKMeans -N 3 -A "weka.core.EuclideanDistance -R first-last" -I 500 -S 10

* It looks like this algorithm tries to cluster the data based on the mean values for each attribute. I'm not sure what all of the attributes are referring to, but it looks like cluster 1 has most of the lowest values, cluster 2 has the highest ones, and cluster 3 has the rest in the middle.

2) Discretize the attributes MMIN, MMAX, CACH, CHMIN and CHMAX using 3 buckets in one step. Use the binning by frequency approach. Find associations among these attributes only (i.e., remove the other ones), using the Apriori algorithm, with support 0.1, confidence 0.95 and top 4 rules only being displayed. Insert the results in your report.

    Discretize -B 3 -M -1.0 -R 2-6
    Remove -R 1,7
    Apriori -N 4 -T 0 -C 0.95 -D 0.05 -U 1.0 -M 0.1 -S -1.0 -c -1

    Best rules found:
    1. CACH='(-inf-85.333333]' 194 ==> CHMIN='(-inf-17.333333]' 193    conf:(0.99)
    2. MMIN='(-inf-10709.333333]' CACH='(-inf-85.333333]' 189 ==> CHMIN='(-inf-17.333333]' 188  
       conf:(0.99)
    3. CACH='(-inf-85.333333]' CHMAX='(-inf-58.666667]' 189 ==> CHMIN='(-inf-17.333333]' 188    
       conf:(0.99)
    4. CHMAX='(-inf-58.666667]' 197 ==> CHMIN='(-inf-17.333333]' 195    conf:(0.99)


3) Using the original CPU dataset, list the eigenvalues associated with the attributes selected by the Principal Components Analysis method, when the amount of variance you wish covered by the subset of attributes is 75%.

    PrincipalComponents -R 0.75 -A 5
    Ranker -T -1.7976931348623157E308 -N -1

    eigenvalue  proportion  cumulative
      3.35674     0.55946     0.55946   0.469MMAX+0.435CHMIN+0.429CACH+0.427MMIN+0.374CHMAX...
      0.82936     0.13823     0.69768   -0.682MYCT-0.559CHMAX+0.333MMIN-0.275CHMIN-0.152CACH...
      0.73923     0.1232      0.82089   -0.669MYCT-0.548MMIN+0.426CHMAX-0.264MMAX+0.03 CHMIN...


