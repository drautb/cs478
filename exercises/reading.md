Reading Homework
================

Ben Draut / CS 478 / Nov. 23, 2013

## Article 1 - Community Structure

**Summary:** This article presented an simple and efficient algorithm for
discovering community structures in a network that is not completely known to
begin with. It works by starting from some root vertex and adding it to the
"community." All nodes outside of the community that connect to it are the
neighborhood. Nodes in the neighborhood are explored and added to the community
in order of the fewest number of connections outside the community. The article
shows that this is an effective and efficient method in both computer generated
graphs as well as real-world networks, such as Amazon.com's recommendation
network.

**Question:** This article made me ask myself where this would have
applications in my life, or in subjects that I am interested in.

**Suggestion:** As an improvement to the article, I would suggest including
additional discussion about applications of discovering community-structure in
networks.

## Article 2 - Social Circles

**Summary:** This article focuses on discovering specific social cirlces (as
opposed to unstructured communities) in directed graphs. It spends some time
discussing why this is important, and how it differs from the problem of simply
finding communities. For example, given the same individual as a starting
point, a different social circle will be discovered if a few family members are
given as initial connections, as opposed to work-related connections. The
article also reduces the problem to simply maximizing the density of a
particular subgraph. It also addresses potential pitfalls in this problem, ways
of choosing the initial query data, and comparisons to other algorithms.

**Question:** This article made me ask myself about the implications of this
for online advertising. I wonder if marketing companies such as Google are
already using this kind of technology to improve their targeting methods for
advertisements.

**Suggestion:** A potential improvement to the work done in this article could
be to expand the work to deal with undirected graphs as well, such as
determining social circles in media such as facebook.



