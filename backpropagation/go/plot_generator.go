package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

type Network struct {
	LearningRate float64
	Momentum     float64
	Layers       []int
	EpochData    []struct {
		N        int
		Training float64
		Test     float64
	}
}

func main() {
	if len(os.Args) != 3 {
		panic("Not enough arguments!")
	}

	iris_learning_rates := []float64{0.1, 0.3, 0.5, 0.8, 1.0, 2.0}
	vowel_learning_rates := []float64{0.1, 0.3, 0.5, 0.8, 1.0, 2.0}

	iris_nodes := []int{2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}
	vowel_nodes := []int{14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34}

	iris_momentums := []float64{0.1, 0.3, 0.5, 0.8, 1.0, 2.0}
	vowel_momentums := []float64{0.1, 0.3, 0.5, 0.8, 1.0, 2.0}

	if os.Args[1] == "iris" {
		if os.Args[2] == "lr" {
			for r := 0; r < len(iris_learning_rates); r++ {
				o, _ := exec.Command("./MLSystemManagerDbg", "-R", "0", "-L", "neuralnet", "-A", "../../../datasets/iris.arff", "-E", "random", "0.7", "-N", "-Layers", "[", "10", "]", "-LR", strconv.FormatFloat(iris_learning_rates[r], 'f', -1, 64), "-M", "0.0").Output()
				output := string(o)
				lines := strings.Split(output, "\n")
				json_string := lines[18]
				generate_plot_code(json_string)
			}
		} else if os.Args[2] == "n" {
			for r := 0; r < len(iris_nodes); r++ {
				o, _ := exec.Command("./MLSystemManagerDbg", "-R", "0", "-L", "neuralnet", "-A", "../../../datasets/iris.arff", "-E", "random", "0.7", "-N", "-Layers", "[", strconv.Itoa(iris_nodes[r]), "]", "-LR", "0.3", "-M", "0.0").Output()
				output := string(o)
				lines := strings.Split(output, "\n")
				json_string := lines[18]
				generate_plot_code(json_string)
			}
		} else if os.Args[2] == "m" {
			for r := 0; r < len(iris_momentums); r++ {
				o, _ := exec.Command("./MLSystemManagerDbg", "-R", "0", "-L", "neuralnet", "-A", "../../../datasets/iris.arff", "-E", "random", "0.7", "-N", "-Layers", "[", "10", "]", "-LR", "0.3", "-M", strconv.FormatFloat(iris_momentums[r], 'f', -1, 64)).Output()
				output := string(o)
				lines := strings.Split(output, "\n")
				json_string := lines[18]
				generate_plot_code(json_string)
			}
		}
	} else if os.Args[1] == "vowel" {
		if os.Args[2] == "lr" {
			for r := 0; r < len(vowel_learning_rates); r++ {
				o, _ := exec.Command("./MLSystemManagerDbg", "-R", "0", "-L", "neuralnet", "-A", "../../../datasets/vowel-bp.arff", "-E", "random", "0.75", "-N", "-Layers", "[", "20", "]", "-LR", strconv.FormatFloat(vowel_learning_rates[r], 'f', -1, 64), "-M", "0.0").Output()
				output := string(o)
				lines := strings.Split(output, "\n")
				json_string := lines[18]
				generate_plot_code(json_string)
			}
		} else if os.Args[2] == "n" {
			for r := 0; r < len(vowel_nodes); r++ {
				o, _ := exec.Command("./MLSystemManagerDbg", "-R", "0", "-L", "neuralnet", "-A", "../../../datasets/vowel-bp.arff", "-E", "random", "0.75", "-N", "-Layers", "[", strconv.Itoa(vowel_nodes[r]), "]", "-LR", "0.5", "-M", "0.0").Output()
				output := string(o)
				lines := strings.Split(output, "\n")
				json_string := lines[18]
				generate_plot_code(json_string)
			}
		} else if os.Args[2] == "2" {
			o, _ := exec.Command("./MLSystemManagerDbg", "-R", "0", "-L", "neuralnet", "-A", "../../../datasets/vowel-bp.arff", "-E", "random", "0.75", "-N", "-Layers", "[", "6", "4", "]", "-LR", "0.5", "-M", "0.0").Output()
			output := string(o)
			lines := strings.Split(output, "\n")
			json_string := lines[18]
			generate_plot_code(json_string)
		} else if os.Args[2] == "m" {
			for r := 0; r < len(vowel_momentums); r++ {
				o, _ := exec.Command("./MLSystemManagerDbg", "-R", "0", "-L", "neuralnet", "-A", "../../../datasets/vowel-bp.arff", "-E", "random", "0.75", "-N", "-Layers", "[", "24", "]", "-LR", "0.5", "-M", strconv.FormatFloat(vowel_momentums[r], 'f', -1, 64)).Output()
				output := string(o)
				lines := strings.Split(output, "\n")
				json_string := lines[18]
				generate_plot_code(json_string)
			}
		}
	}
}

func generate_plot_code(json_str string) {
	network := &Network{}
	err := json.Unmarshal([]byte(json_str), &network)
	if err != nil {
		panic(err)
	}

	fmt.Print("training = [ ")
	for e := 0; e < len(network.EpochData); e++ {
		fmt.Print(network.EpochData[e].Training)
		fmt.Print(" ")
	}
	fmt.Print("];\n")

	fmt.Print("test = [ ")
	for e := 0; e < len(network.EpochData); e++ {
		fmt.Print(network.EpochData[e].Test)
		fmt.Print(" ")
	}
	fmt.Print("];\n")

	fmt.Print("epochs = 1:1:")
	lastEpoch := network.EpochData[len(network.EpochData)-1].N
	fmt.Printf("%d;\n\n", lastEpoch)

	fmt.Printf("WIDTH = %d;\n\n", lastEpoch)

	fmt.Println("figure;")
	fmt.Println("figureHandle = gcf;")

	fmt.Println("plot(epochs(1:WIDTH), training(1:WIDTH), 'r', epochs(1:WIDTH), test(1:WIDTH), 'b');")

	fmt.Println("xlabel('Epoch');")
	fmt.Println("ylabel('Accuracy');")

	if os.Args[1] == "vowel" && os.Args[2] == "2" {
		fmt.Printf("title('Accuracy vs. Time (%d Hidden Layers, %d Nodes and %d Nodes, LR = %1.1f, M = %1.1f)');\n", len(network.Layers), network.Layers[0], network.Layers[1], network.LearningRate, network.Momentum)
	} else {
		fmt.Printf("title('Accuracy vs. Time (%d Hidden Layer, %d Nodes, LR = %1.1f, M = %1.1f)');\n", len(network.Layers), network.Layers[0], network.LearningRate, network.Momentum)
	}
	fmt.Println("legend('Training Data', 'Test Data', 'Location', 'Best');\n")
	fmt.Println("set(findall(figureHandle, 'type', 'text'), 'fontSize', 14, 'fontWeight', 'bold');")

	if os.Args[1] == "iris" {
		if os.Args[2] == "lr" {
			fmt.Printf("print(figureHandle, '../../../Images/iris-lr-%1.1f.png', '-dpng');\n", network.LearningRate)
		} else if os.Args[2] == "n" {
			fmt.Printf("print(figureHandle, '../../../Images/iris-n-%d.png', '-dpng');\n", network.Layers[0])
		} else if os.Args[2] == "m" {
			fmt.Printf("print(figureHandle, '../../../Images/iris-m-%1.1f.png', '-dpng');\n", network.Momentum)
		}
	} else if os.Args[1] == "vowel" {
		if os.Args[2] == "lr" {
			fmt.Printf("print(figureHandle, '../../../Images/vowel-lr-%1.1f.png', '-dpng');\n", network.LearningRate)
		} else if os.Args[2] == "n" {
			fmt.Printf("print(figureHandle, '../../../Images/vowel-n-%d.png', '-dpng');\n", network.Layers[0])
		} else if os.Args[2] == "2" {
			fmt.Printf("print(figureHandle, '../../../Images/vowel-2layer.png', '-dpng');\n")
		} else if os.Args[2] == "m" {
			fmt.Printf("print(figureHandle, '../../../Images/vowel-m-%1.1f.png', '-dpng');\n", network.Momentum)
		}
	}

	fmt.Println()
}
