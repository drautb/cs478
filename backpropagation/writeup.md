<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<style type="text/css">
.img-container {
    text-align: center;
}

.img-container img.thirty {
    width: 33%;
}

.img-container img.fifty {
    width: 50%;
}

</style>
<style type="text/css">
body {
    font-size: 10pt;
}
</style>

Backpropagation Project
=======================

Ben Draut / CS 478 / Oct. 24, 2013

_My network utilized three stopping criteria: A time limit, a maximum epoch limit, and a threshold for a minimum change in error. In all tests, training terminated because the change in error dipped beneath the threshold._

## Iris Analysis

_**Use your backpropagation algorithm on the Iris problem, with a random 70/30 split.**_

**Learning Rates**

<div class="img-container">
    <img class="thirty" src="../../Images/iris-lr-0.100000.png"/>
    <img class="thirty" src="../../Images/iris-lr-0.300000.png"/>
    <img class="thirty" src="../../Images/iris-lr-0.500000.png"/>
    <img class="thirty" src="../../Images/iris-lr-0.800000.png"/>
    <img class="thirty" src="../../Images/iris-lr-1.000000.png"/>
    <img class="thirty" src="../../Images/iris-lr-2.000000.png"/>
</div>

Best Learning Rate: $0.3$

**Hidden Layer Node Counts**

<div class="img-container">
    <img class="thirty" src="../../Images/iris-n-2.png"/>
    <img class="thirty" src="../../Images/iris-n-4.png"/>
    <img class="thirty" src="../../Images/iris-n-6.png"/>
    <img class="thirty" src="../../Images/iris-n-8.png"/>
    <img class="thirty" src="../../Images/iris-n-10.png"/>
    <img class="thirty" src="../../Images/iris-n-12.png"/>
</div>

Best Number of Hidden Nodes: $10$

## Vowel Analysis

_**Use your backpropagation algorithm on the Vowel problem, with a random 75/25 split. (Note: Make sure you ignore the "Train or Test" attribute).**_

**Learning Rates**

<div class="img-container">
    <img class="thirty" src="../../Images/vowel-lr-0.100000.png"/>
    <img class="thirty" src="../../Images/vowel-lr-0.300000.png"/>
    <img class="thirty" src="../../Images/vowel-lr-0.500000.png"/>
    <img class="thirty" src="../../Images/vowel-lr-0.800000.png"/>
    <img class="thirty" src="../../Images/vowel-lr-1.000000.png"/>
    <img class="thirty" src="../../Images/vowel-lr-2.000000.png"/>
</div>

Best Learning Rate: $0.5$

**Hidden Layer Node Counts**

<div class="img-container">
    <img class="thirty" src="../../Images/vowel-n-18.png"/>
    <img class="thirty" src="../../Images/vowel-n-20.png"/>
    <img class="thirty" src="../../Images/vowel-n-22.png"/>
    <img class="thirty" src="../../Images/vowel-n-24.png"/>
    <img class="thirty" src="../../Images/vowel-n-26.png"/>
    <img class="thirty" src="../../Images/vowel-n-28.png"/>
</div>

Best Number of Hidden Layer Nodes: $24$

**2-Layer Network (6, 4)**

<div class="img-container">
    <img class="fifty" src="../../Images/vowel-2layer.png"/>
</div>

## Momentum Analysis

### Iris

<div class="img-container">
    <img class="thirty" src="../../Images/iris-m-0.1.png"/>
    <img class="thirty" src="../../Images/iris-m-0.3.png"/>
    <img class="thirty" src="../../Images/iris-m-0.5.png"/>
    <img class="thirty" src="../../Images/iris-m-0.8.png"/>
    <img class="thirty" src="../../Images/iris-m-1.0.png"/>
    <img class="thirty" src="../../Images/iris-m-2.0.png"/>
</div>

### Vowel

<div class="img-container">
    <img class="thirty" src="../../Images/vowel-m-0.1.png"/>
    <img class="thirty" src="../../Images/vowel-m-0.3.png"/>
    <img class="thirty" src="../../Images/vowel-m-0.5.png"/>
    <img class="thirty" src="../../Images/vowel-m-0.8.png"/>
    <img class="thirty" src="../../Images/vowel-m-1.0.png"/>
    <img class="thirty" src="../../Images/vowel-m-2.0.png"/>
</div>

## Discussion

**Discuss the effect of different learning rates on the algorithm's performance.**

The learning rate affected how quickly the network would develop. Higher learning rates improved accuracy sooner, but were more susceptible to instability and variation. Lower learning rates took longer to improve, but their performance was much more consistent. (Meaning that once there were no sudden spikes or drops in performance.)

**Discuss the effect of different numbers of hidden units on the algorithm's performance (1-hidden layer case).**

As the number of hidden nodes increased, the variation in accuracy between epochs decreased. Performance was more consistent. The number of hidden nodes also affected how accurate a network could become. In order for a model to achieve a high accuracy, it seemed to need at least the same number of hidden nodes as input nodes, if not more. Most tests seemed to do the best with approximately twice that many.

**Compare your recorded best numbers of hidden nodes for each problem with the following heuristic value: $H=\frac{N}{10(I+O)}$, where $N$ is the size of the (training) data set, $I$ is the number of network inputs and $O$ is the number of outputs.**

**Iris:** $H = \frac{105}{10(4+3)} = 1.5$. The best number of hidden nodes was 10 according to my tests. 

**Vowel:** $H = \frac{743}{10(12+11)} = 3.23$ The best number of hidden nodes was 24 according to my tests.

This heuristic estimates a number far lower than the best values I discovered empirically. I could see this heuristic giving better values on larger datasets, (more training instances) but on these two datasets, it was far smaller than I expected.

**How did the momentum term affect the learner's behavior (number of epochs to convergence, final accuracy, etc.)?**

The momentum term helped the accuracy improve very quickly, requiring fewer epochs to terminate. It also increasd the variation between accuracies from epoch to epoch however, which tended to lead to inconsistent accuracies. (Typically for higher momentums) With some tweaking, the momentum term could significantly reduce the training time, while achieving high accuracy on the test data.

